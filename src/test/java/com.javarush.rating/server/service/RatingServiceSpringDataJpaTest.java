package com.javarush.rating.server.service;

import com.javarush.rating.client.RatingService;
import com.javarush.rating.client.ratingtype.SortRatingType;
import com.javarush.rating.client.ratingtype.TimeRatingType;
import com.javarush.rating.shared.dto.User;
import com.javarush.rating.shared.helpers.RatingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.javarush.rating.client.ratingtype.SortRatingType.*;
import static com.javarush.rating.client.ratingtype.TimeRatingType.TOTAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertTrue;

/**
 * @author Timur
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@Transactional
public class RatingServiceSpringDataJpaTest extends AbstractTestNGSpringContextTests {

    private final Comparator<User> solvedRatingComp = new Comparator<User>() {
        @Override
        public int compare(User user1, User user2) {
            return (user1.getSolvedRating() > user2.getSolvedRating()) ? 1 : -1;
        }
    };
    private final Comparator<User> levelRatingComp = new Comparator<User>() {
        @Override
        public int compare(User user1, User user2) {
            return (user1.getLvlRating() > user2.getLvlRating()) ? 1 : -1;
        }
    };
    private final Comparator<User> averageRatingComp = new Comparator<User>() {
        @Override
        public int compare(User user1, User user2) {
            return (user1.getAvgRating() > user2.getAvgRating()) ? 1 : -1;
        }
    };

    @Autowired
    private RatingService ratingService;

    @DataProvider(name = "startsWith")
    public static Object[][] startsWith() {
        return new Object[][]{
                {"Iva"}, {"Сер"}, {"Але"}, {"Tim"}
        };
    }

    // TODO add MONTH, WEEK data
    @DataProvider(name = "testData")
    public Object[][] testData() {
        return new Object[][]{
                {"Сергей Кандалинцев", 149, SOLVED, TOTAL, solvedRatingComp},
                {"Женя Кашера", 9723, LEVEL, TOTAL, levelRatingComp},
                {"Timur Ibraev", 10896, SOLVED, TOTAL, solvedRatingComp},
                {"Илья Альтерович", 12810, AVERAGE, TOTAL, averageRatingComp}
        };
    }

    @Test(dataProvider = "startsWith")
    public void testFindByDisplayNameStartsWith(final String name) throws Exception {
        List<String> users = ratingService.findSuggestionsByName(name, TOTAL);
        assertThat(users).isNotNull();
        assertThat(users).isNotEmpty();
        assertThat(users.size()).isLessThanOrEqualTo(RatingConfig.PAGE_SIZE);

        for (String user : users) {
            assertTrue(user.startsWith(name), "Name must starts with: [" + name + "]");
        }
    }

    @Test(dataProvider = "testData")
    public void testFindPageByNameBySortingRatingTypeAndTimeRatingType(String name, int userId, SortRatingType sortRatingType,
                                                                       TimeRatingType timeRatingType, Comparator<User> comparator) throws Exception {
        ArrayList<User> page = ratingService.findPageByName(name, sortRatingType, timeRatingType);
        assertThat(page).isNotNull();
        assertThat(page).isNotEmpty();
        assertThat(page.size()).isLessThanOrEqualTo(RatingConfig.PAGE_SIZE);
        assertThat(page).extracting("id", Integer.class).contains(userId);
        assertThat(page).extracting("displayName", String.class).contains(name);
        assertThat(page).isSortedAccordingTo(comparator);
    }

}
