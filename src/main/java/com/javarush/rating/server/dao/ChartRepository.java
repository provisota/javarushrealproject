package com.javarush.rating.server.dao;

import com.javarush.rating.shared.domain.TotalRating;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Alterovych Ilya
 */
public interface ChartRepository extends BaseRepository<TotalRating, Integer> {

    @Query("select tr.level, count(tr) " +
            "from TotalRating tr " +
            "group by tr.level " +
            "order by tr.level")
    ArrayList<Object[]> findCountUsersOnLevels();

    @Query("SELECT ts.level, SUM(ts.attemptUnsolved), SUM(ts.attemptSolved), SUM(ts.usrSolved) " +
            "FROM TaskStatsEntity as ts " +
            "group by ts.level")
    ArrayList<Object[]> findAvgAttemptsOnLevels();

    @Query("SELECT date(se.createdTime), count(se) " +
            "FROM StatisticEntity as se " +
            "where se.actionType = (?1) and se.createdTime between (?2) and (?3)" +
            "group by date(se.createdTime)")
    ArrayList<Object[]> findActionCountByDate(Integer actionType, Date startDate, Date endDate);
}