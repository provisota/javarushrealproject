package com.javarush.rating.server.dao;

import com.javarush.rating.shared.domain.TotalRating;

/**
 * Интерфейс для операций чтения общих рейтингов.
 *
 * @author Timur
 */
public interface TotalRatingRepository extends BaseRepository<TotalRating, Integer> {
}
