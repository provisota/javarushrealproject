package com.javarush.rating.server.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Интерфейс для операций чтения определенного типа.
 *
 * @param <T>  тип <em>Entity</em> класса
 * @param <ID> тип id определенного <em>Entity</em> класса
 * @author Timur
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends Repository<T, ID> {

    /**
     * Находит список совпадений по имени пользователя.
     *
     * <p/>
     * Пример :
     * <pre>
     * {@literal @}Autowired
     * private TotalRatingRepository repository;
     * // MySQL LIMIT 0, 10;
     * PageRequest request = new PageRequest(0, 10);
     * ArrayList&lt;String&gt; list = repository.findByDisplayNameStartsWith("Иван", request);
     *
     * Hibernate сгенерирует этот запрос:
     *     select
     *         totalratin0_.displayName as col_0_0_
     *     from
     *         total_rating totalratin0_
     *     where
     *         totalratin0_.displayName like ? limit ?
     * </pre>
     *
     * @param displayName имя пользователя с которого будет начинаться поиск совпадений.
     * @param pageable    pagination information, используется для установки лимита возвращаемых записей из БД.
     * @return список имен пользователей начинающихся с displayName
     */
    @Query("select r.displayName from #{#entityName} r where r.displayName like ?1%")
    ArrayList<String> findByDisplayNameStartsWith(String displayName, Pageable pageable);

    /**
     * Находит entity по его имени пользователя.
     *
     * @param displayName имя пользователя.
     * @return entity c данным именем пользователя или {@literal null} если не найдено
     */
    T findOneByDisplayName(String displayName);

    /**
     * Retrieves an entity by its id.
     *
     * @param id must not be {@literal null}.
     * @return the entity with the given id or {@literal null} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}
     */
    T findOne(ID id);

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param id must not be {@literal null}.
     * @return true if an entity with the given id exists, {@literal false} otherwise
     * @throws IllegalArgumentException if {@code id} is {@literal null}
     */
    boolean exists(ID id);

    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    Iterable<T> findAll();

    /**
     * Returns all instances of the type with the given IDs.
     *
     * @param ids ids
     * @return all entities
     */
    Iterable<T> findAll(Iterable<ID> ids);

    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param pageable pagination information.
     * @return a page of entities
     */
    Page<T> findAll(Pageable pageable);

    /**
     * Returns all entities sorted by the given options.
     *
     * @param sort {@link Sort}
     * @return all entities sorted by the given options
     */
    Iterable<T> findAll(Sort sort);

    /**
     * Returns the number of entities available.
     *
     * @return the number of entities
     */
    long count();
}
