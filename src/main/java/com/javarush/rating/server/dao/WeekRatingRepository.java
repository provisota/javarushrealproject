package com.javarush.rating.server.dao;

import com.javarush.rating.shared.domain.WeekRating;

/**
 * Интерфейс для операций чтения недельных рейтингов.
 *
 * @author Timur
 */
public interface WeekRatingRepository extends BaseRepository<WeekRating, Integer> {
}
