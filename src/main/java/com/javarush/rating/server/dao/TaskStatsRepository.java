package com.javarush.rating.server.dao;

import com.javarush.rating.shared.domain.TaskStatsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.ArrayList;

/**
 * @author Sergey Kandalintsev
 */
public interface TaskStatsRepository extends Repository<TaskStatsEntity, Integer> {
    ArrayList<TaskStatsEntity> findByLevel(int level);

    @Query("SELECT DISTINCT ts.level FROM TaskStatsEntity ts")
    ArrayList<Integer> findAllLevels();
}
