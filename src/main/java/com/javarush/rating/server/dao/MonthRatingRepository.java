package com.javarush.rating.server.dao;

import com.javarush.rating.shared.domain.MonthRating;

/**
 * Интерфейс для операций чтения месячных рейтингов.
 *
 * @author Timur
 */
public interface MonthRatingRepository extends BaseRepository<MonthRating, Integer> {
}
