package com.javarush.rating.server.service;

import com.javarush.rating.client.TaskStatsService;
import com.javarush.rating.server.dao.TaskStatsRepository;
import com.javarush.rating.shared.domain.TaskStatsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Date: 29.01.14
 *
 * @author Sergey Kandalintsev
 */
@Service("taskStatsService")
@Repository
@Transactional(readOnly = true)
public class TaskStatsServiceImpl implements TaskStatsService {

    @Autowired
    private TaskStatsRepository taskStatsRepository;

    @Override
    public ArrayList<Integer> findAllLevels() {
        return taskStatsRepository.findAllLevels();
    }

    @Override
    public ArrayList<TaskStatsEntity> findByLevel(int level) {
        return taskStatsRepository.findByLevel(level);
    }
}
