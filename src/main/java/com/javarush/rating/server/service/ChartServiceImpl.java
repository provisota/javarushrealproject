package com.javarush.rating.server.service;

import com.javarush.rating.client.ChartService;
import com.javarush.rating.server.dao.ChartRepository;
import com.javarush.rating.shared.dto.ActionByDateChartBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
@Service("chartService")
@Repository
@Transactional(readOnly = true)
public class ChartServiceImpl implements ChartService {
    @Autowired
    private ChartRepository chartRepository;

    @Override
    public ArrayList<Integer[]> findCountUsersOnLevels() {
        List<Object[]> results = chartRepository.findCountUsersOnLevels();
        ArrayList<Integer[]> resultList = new ArrayList<Integer[]>();
        for (Object[] obj : results) {
            Integer level = ((Number) obj[0]).intValue();
            Integer usrCount = ((Number) obj[1]).intValue();
            resultList.add(new Integer[]{level, usrCount});
        }
        return resultList;
    }

    @Override
    public ArrayList<Integer[]> findAvgAttemptsOnLevels() {
        List<Object[]> results = chartRepository.findAvgAttemptsOnLevels();
        ArrayList<Integer[]> resultList = new ArrayList<Integer[]>();
        for (Object[] obj : results) {
            Integer level = ((Number) obj[0]).intValue();
            Integer attUns = ((Number) obj[1]).intValue();
            Integer attSol = ((Number) obj[2]).intValue();
            Integer usrSol = ((Number) obj[3]).intValue();
            resultList.add(new Integer[]{level, attUns, attSol, usrSol});
        }
        return resultList;
    }

    @Override
    public ArrayList<ActionByDateChartBean> findActionCountByDate(Integer actionType, Date startDate, Date endDate) {
        List<Object[]> results = chartRepository.findActionCountByDate(actionType, startDate, endDate);
        ArrayList<ActionByDateChartBean> resultList = new ArrayList<ActionByDateChartBean>();
        for (Object[] obj : results) {
            Date chartDate = (Date) obj[0];
            Integer actionCount = ((Number) obj[1]).intValue();
            if ((actionType == 4 && actionCount < 100) ||
                    (actionType == 6 && actionCount < 10))
                continue;
            ActionByDateChartBean returningBean = new ActionByDateChartBean();
            returningBean.setChartDate(chartDate);
            returningBean.setActionCount(actionCount);
            resultList.add(returningBean);
        }
        return resultList;
    }
}
