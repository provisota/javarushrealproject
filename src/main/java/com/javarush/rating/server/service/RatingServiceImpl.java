package com.javarush.rating.server.service;

import com.javarush.rating.client.RatingService;
import com.javarush.rating.client.ratingtype.SortRatingType;
import com.javarush.rating.client.ratingtype.TimeRatingType;
import com.javarush.rating.server.dao.MonthRatingRepository;
import com.javarush.rating.server.dao.TotalRatingRepository;
import com.javarush.rating.server.dao.WeekRatingRepository;
import com.javarush.rating.shared.domain.MonthRating;
import com.javarush.rating.shared.domain.RatingEntity;
import com.javarush.rating.shared.domain.TotalRating;
import com.javarush.rating.shared.domain.WeekRating;
import com.javarush.rating.shared.dto.User;
import com.javarush.rating.shared.helpers.RatingConfig;
import com.javarush.rating.shared.helpers.UserStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * @author Timur
 */
@Service("ratingService")
@Repository
public class RatingServiceImpl implements RatingService {

    private final TotalRatingRepository totalRatingRepository;
    private final MonthRatingRepository monthRatingRepository;
    private final WeekRatingRepository weekRatingRepository;

    @Autowired
    public RatingServiceImpl(final TotalRatingRepository totalRatingRepository,
                             final MonthRatingRepository monthRatingRepository,
                             final WeekRatingRepository weekRatingRepository) {
        this.totalRatingRepository = totalRatingRepository;
        this.monthRatingRepository = monthRatingRepository;
        this.weekRatingRepository = weekRatingRepository;
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable(value = "rating")
    public final ArrayList<User> findAllByPage(TimeRatingType timeRatingType, int page, SortRatingType sortRatingType) {
        Pageable pageable = new PageRequest(page, RatingConfig.PAGE_SIZE, Sort.Direction.ASC, sortRatingType.toString());

        switch (timeRatingType) {
            case TOTAL:
                Page<TotalRating> totalRatings = totalRatingRepository.findAll(pageable);
                return User.convert(new ArrayList<>(totalRatings.getContent()));
            case MONTH:
                Page<MonthRating> monthRatings = monthRatingRepository.findAll(pageable);
                return User.convert(new ArrayList<>(monthRatings.getContent()));
            case WEEK:
                Page<WeekRating> weekRatings = weekRatingRepository.findAll(pageable);
                return User.convert(new ArrayList<>(weekRatings.getContent()));
            default:
                return new ArrayList<>(0);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public final ArrayList<String> findSuggestionsByName(String startsWith, TimeRatingType timeRatingType) {
        PageRequest request = new PageRequest(0, RatingConfig.PAGE_SIZE);
        switch (timeRatingType) {
            case TOTAL:
                return totalRatingRepository.findByDisplayNameStartsWith(startsWith, request);
            case MONTH:
                return monthRatingRepository.findByDisplayNameStartsWith(startsWith, request);
            case WEEK:
                return weekRatingRepository.findByDisplayNameStartsWith(startsWith, request);
            default:
                return new ArrayList<>(0);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public final ArrayList<User> findPageByName(final String name, SortRatingType sortRatingType, TimeRatingType timeRatingType) {
        if (name == null || name.length() < RatingConfig.INPUT_STR_LENGTH) {
            return new ArrayList<>(0);
        }

        RatingEntity ratingEntity = null;

        switch (timeRatingType) {
            case TOTAL:
                ratingEntity = totalRatingRepository.findOneByDisplayName(name);
                break;
            case MONTH:
                ratingEntity = monthRatingRepository.findOneByDisplayName(name);
                break;
            case WEEK:
                ratingEntity = weekRatingRepository.findOneByDisplayName(name);
                break;
        }
        int userId = (ratingEntity != null) ? ratingEntity.getId() : 0;
        return (userId != 0) ? findById(userId, sortRatingType, timeRatingType) : new ArrayList<User>(0);
    }

    @Override
    @Transactional(readOnly = true)
    public final long getTotalPages(TimeRatingType timeRatingType) {
        switch (timeRatingType) {
            case TOTAL:
                return totalRatingRepository.count();
            case MONTH:
                return monthRatingRepository.count();
            case WEEK:
                return weekRatingRepository.count();
            default:
                return 0L;
        }
    }

    private ArrayList<User> findById(int id, SortRatingType sortRatingType, TimeRatingType timeRatingType) {

        final UserStatistics userStatistics = getUserStatistics(id, timeRatingType);

        if (userStatistics == null) {
            return new ArrayList<>(0);
        }

        switch (sortRatingType) {
            case SOLVED:
                return findAllByPage(timeRatingType, RatingConfig.SOLVED.getPage(userStatistics), SortRatingType.SOLVED);
            case LEVEL:
                return findAllByPage(timeRatingType, RatingConfig.LEVEL.getPage(userStatistics), SortRatingType.LEVEL);
            case AVERAGE:
                return findAllByPage(timeRatingType, RatingConfig.AVERAGE.getPage(userStatistics), SortRatingType.AVERAGE);
            default:
                return new ArrayList<>(0);
        }
    }

    private UserStatistics getUserStatistics(int id, TimeRatingType timeRatingType) {
        switch (timeRatingType) {
            case TOTAL:
                return UserStatistics.valueOf(totalRatingRepository.findOne(id));
            case MONTH:
                return UserStatistics.valueOf(monthRatingRepository.findOne(id));
            case WEEK:
                return UserStatistics.valueOf(weekRatingRepository.findOne(id));
            default:
                return null;
        }
    }
}
