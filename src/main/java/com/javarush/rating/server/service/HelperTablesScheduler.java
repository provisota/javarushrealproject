package com.javarush.rating.server.service;

import org.springframework.cache.annotation.CacheEvict;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Sergey Kandalintsev
 */
public class HelperTablesScheduler {

    @PersistenceContext
    private EntityManager em;

    @CacheEvict(value = "rating", allEntries = true)
    public void callHelperTables() {
        em.createStoredProcedureQuery("updateHelperTables").execute();
    }
}
