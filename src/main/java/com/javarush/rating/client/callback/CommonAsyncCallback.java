package com.javarush.rating.client.callback;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.javarush.rating.client.RatingApplication;

public abstract class CommonAsyncCallback<T> implements AsyncCallback<T> {
    @Override
    public void onFailure(Throwable caught) {
        RatingApplication.displayErrorToUser(caught);
    }
}
