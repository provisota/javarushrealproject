package com.javarush.rating.client.view.tasks.columns;

import com.google.gwt.user.cellview.client.DataGrid;
import com.javarush.rating.shared.domain.TaskStatsEntity;

/**
 * @author Alterovych Ilya
 *         Date: 08.02.14
 */
class MaxAttemptsColumn extends AbstractTaskColumn {

    public MaxAttemptsColumn(DataGrid<TaskStatsEntity> dataGrid, String name, String title, int width) {
        super(dataGrid, name, title, width);
    }

    @Override
    public String getValue(TaskStatsEntity taskObject) {
        return String.valueOf(taskObject.getMaxAttempts());
    }
}