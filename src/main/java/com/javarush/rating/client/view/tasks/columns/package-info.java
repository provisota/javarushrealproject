/**
 * Содержит в себе классы описывающие колонки
 * в таблице (DataGrid) статистики задач.
 * @author Alterovych Ilya
 */
package com.javarush.rating.client.view.tasks.columns;