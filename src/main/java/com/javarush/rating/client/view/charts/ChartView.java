package com.javarush.rating.client.view.charts;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.visualization.client.AbstractDataTable;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.corechart.ColumnChart;
import com.google.gwt.visualization.client.visualizations.corechart.Options;
import com.javarush.rating.client.presenter.ChartPresenter;
import com.javarush.rating.shared.dto.ActionByDateChartBean;
import org.gwtbootstrap3.client.ui.ListBox;
import org.gwtbootstrap3.client.ui.Tooltip;
import org.gwtbootstrap3.client.ui.constants.Placement;

import java.util.ArrayList;

/**
 * @author Alterovych Ilya
 *         Date: 10.03.14
 */
public class ChartView extends Composite {
    private static ChartViewUiBinder ourUiBinder = GWT.create(ChartViewUiBinder.class);
    private ColumnChart columnChart;
    private ChartPresenter chartPresenter;
    private ChartDateBox chartDateBox;
    private HorizontalPanel dateBoxHolderPanel;
    private ChartType selectedChartType;

    @UiField(provided = true)
    ListBox chartListBox;
    @UiField
    VerticalPanel chartHolderPanel;

    @UiConstructor
    public ChartView(ChartPresenter chartPresenter) {
        this.chartPresenter = chartPresenter;
    }

    @UiHandler("chartListBox")
    public void setTasksListBoxListener(ChangeEvent event) {
        selectedChartType = ChartType.getBySelectedIndex
                (chartListBox.getSelectedIndex());
        if (selectedChartType == ChartType.CHART_3 || selectedChartType == ChartType.CHART_4)
            chartDateBox.reset();
        chartPresenter.displayChart(selectedChartType);
    }

    public void init() {
        setupListBox();
        setupDateBox();

        Runnable onLoadCallback = new Runnable() {
            public void run() {
                // Create a column chart visualization.
                columnChart = new ColumnChart(createTable(ChartType.CHART_1),
                        createOptions(ChartType.CHART_1));
                chartHolderPanel.add(columnChart);
            }
        };
        VisualizationUtils.loadVisualizationApi(onLoadCallback, ColumnChart.PACKAGE);
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    public void refreshTable(ChartType chartType) {
        if (chartType == ChartType.CHART_1) {
            chartHolderPanel.clear();
            columnChart.draw(createTable(chartType), createOptions(chartType));
            chartHolderPanel.add(columnChart);
        }
        if (chartType == ChartType.CHART_2) {
            chartHolderPanel.clear();
            columnChart.draw(createTable(chartType), createOptions(chartType));
            chartHolderPanel.add(columnChart);
        }
        if (chartType == ChartType.CHART_3) {
            chartHolderPanel.clear();
            columnChart.draw(createTable(ChartType.CHART_3), createOptions(ChartType.CHART_3));
            chartHolderPanel.add(dateBoxHolderPanel);
            chartHolderPanel.add(columnChart);
        }
        if (chartType == ChartType.CHART_4) {
            chartHolderPanel.clear();
            columnChart.draw(createTable(ChartType.CHART_4), createOptions(ChartType.CHART_4));
            chartHolderPanel.add(dateBoxHolderPanel);
            chartHolderPanel.add(columnChart);
        }
    }

    private Options createOptions(ChartType chartType) {
        Options options = null;
        //createOptions for ChartType.CHART_1
        if (chartType == ChartType.CHART_1) {
            options = new ChartOptions("уровень", "кол-во пользователей (%)").getOptions();
        }
        //createOptions for ChartType.CHART_2
        if (chartType == ChartType.CHART_2) {
            options = new ChartOptions("уровень", "ср. кол-во попыток").getOptions();
        }
        //createOptions for ChartType.CHART_3
        if (chartType == ChartType.CHART_3) {
            options = new ChartOptions("дата", "кол-во пройденных уровней").getOptions();
        }
        //createOptions for ChartType.CHART_4
        if (chartType == ChartType.CHART_4) {
            options = new ChartOptions("дата", "кол-во решённых задач").getOptions();
        }
        return options;
    }

    private AbstractDataTable createTable(ChartType chartType) {
        DataTable data = null;
        //createTable for ChartType.CHART_1
        if (chartType == ChartType.CHART_1) {
            ArrayList<Integer[]> dataList = chartPresenter.getModel().getResultList();
            Integer totalUsersCount = 0;
            for (Integer[] row : dataList) {
                totalUsersCount += row[1];
            }

            data = DataTable.create();
            data.addColumn(AbstractDataTable.ColumnType.STRING, "уровень");
            data.addColumn(AbstractDataTable.ColumnType.NUMBER, "кол-во пользователей (%)");
            data.addRows(dataList.size());

            for (int i = 0; i < dataList.size(); i++) {
                Integer[] arr = dataList.get(i);
                data.setValue(i, 0, arr[0].toString());
                data.setValue(i, 1, Math.round((double) arr[1] * 100 / (double) totalUsersCount));
            }
        }
        //createTable for ChartType.CHART_2
        if (chartType == ChartType.CHART_2) {
            ArrayList<Integer[]> dataList = chartPresenter.getModel().getResultList();
            data = DataTable.create();
            data.addColumn(AbstractDataTable.ColumnType.NUMBER, "уровень");
            data.addColumn(AbstractDataTable.ColumnType.NUMBER, "ср. кол-во попыток");
            data.addRows(dataList.size());

            NumberFormat nf = NumberFormat.getFormat("##########.00");
            for (int i = 0; i < dataList.size(); i++) {
                Integer[] arr = dataList.get(i);
                data.setValue(i, 0, arr[0]);
                double attValue = ((double) (arr[1] + arr[2]) / (double) arr[3]);
                data.setValue(i, 1, nf.format(attValue));
            }
        }
        //createTable for ChartType.CHART_3
        if (chartType == ChartType.CHART_3) {
            ArrayList<ActionByDateChartBean> dataList = chartPresenter.getModel().getResultBeanList();
            data = DataTable.create();
            data.addColumn(AbstractDataTable.ColumnType.DATE, "дата");
            data.addColumn(AbstractDataTable.ColumnType.NUMBER, "кол-во пройденных уровней");
            data.addRows(dataList.size());

            for (int i = 0; i < dataList.size(); i++) {
                ActionByDateChartBean bean = dataList.get(i);
                data.setValue(i, 0, bean.getChartDate());
                data.setValue(i, 1, bean.getActionCount());
            }
        }
        //createTable for ChartType.CHART_4
        if (chartType == ChartType.CHART_4) {
            ArrayList<ActionByDateChartBean> dataList = chartPresenter.getModel().getResultBeanList();
            data = DataTable.create();
            data.addColumn(AbstractDataTable.ColumnType.DATE, "дата");
            data.addColumn(AbstractDataTable.ColumnType.NUMBER, "кол-во решённых задач");
            data.addRows(dataList.size());

            for (int i = 0; i < dataList.size(); i++) {
                ActionByDateChartBean bean = dataList.get(i);
                data.setValue(i, 0, bean.getChartDate());
                data.setValue(i, 1, bean.getActionCount());
            }
        }
        return data;
    }

    private void setupDateBox() {
        chartDateBox = new ChartDateBox(this);
        dateBoxHolderPanel = chartDateBox.getChartDateBoxPanel();
    }

    private void setupListBox() {
        chartListBox = new ListBox();
        Tooltip tooltip = new Tooltip();
        tooltip.setText("Выберите нужный график");
        tooltip.setPlacement(Placement.RIGHT);
        tooltip.add(chartListBox);
        for (ChartType type : ChartType.values()) {
            chartListBox.addItem(type.getTitleInUIList());
        }
        chartListBox.setWidth("50%");
        chartListBox.setVisibleItemCount(1);
        chartListBox.setSelectedIndex(0);
    }

    interface ChartViewUiBinder extends UiBinder<VerticalPanel, ChartView> {
    }

    public ChartDateBox getChartDateBox() {
        return chartDateBox;
    }

    public ChartType getSelectedChartType() {
        return selectedChartType;
    }

    public ChartPresenter getChartPresenter() {
        return chartPresenter;
    }
}