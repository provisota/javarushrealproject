package com.javarush.rating.client.view.tasks.columns;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.DataGrid;
import com.javarush.rating.shared.domain.TaskStatsEntity;

/**
 * Класс описывающий колонку "Среднее кол-во попыток"
 * в таблице (DataGrid) статистики задач.
 * @author Alterovych Ilya
 */
class AttemptsColumn extends AbstractTaskColumn {
    /**
     * конструктор класса
     *
     * @param dataGrid таблица в которую требуется добавить созданную колонку
     * @param name     название колонки (отображается в заголовке)
     * @param title    всплывающая подсказка (tooltip) для заголовка
     * @param width    ширина колонки
     */
    public AttemptsColumn(final DataGrid<TaskStatsEntity> dataGrid,
                          final String name, final String title,
                          final int width) {
        super(dataGrid, name, title, width);
    }

    /**
     * метод заполняющий конкретную ячейку в колонке "Среднее кол-во попыток"
     * значеним полученным из сущности (строки) TaskStatsEntity представляющую
     * конкретную задачу.
     *
     * @param taskObject сущность (строка) TaskStatsEntity представляющая
     *                   конкретную задачу.
     * @return значение ячейки, полученное из <em>taskObject</em>
     */
    @Override
    public String getValue(final TaskStatsEntity taskObject) {
        NumberFormat nf = NumberFormat.getFormat("##########.00");
        if (taskObject.getAttemptSolved() == 0) {
            return "--";
        } else {
            return String.valueOf(nf.format((taskObject.getAttemptUnsolved().doubleValue()
                    + taskObject.getAttemptSolved().doubleValue())
                    / taskObject.getUsrSolved().doubleValue()));
        }
    }
}
