

package com.javarush.rating.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import com.javarush.rating.client.presenter.ChartPresenter;
import com.javarush.rating.client.presenter.TaskPresenter;
import com.javarush.rating.client.presenter.UserPresenter;
import com.javarush.rating.client.presenter.WidgetHolderPresenter;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.ButtonType;

public class WidgetHolder extends DockPanel {

    private UserPresenter userPresenter;
    private TaskPresenter taskPresenter;
    private ChartPresenter chartPresenter;

    private Button usersButton;
    private Button tasksButton;
    private Button chartButton;

    VerticalPanel panel;

    private VerticalPanel centerHolder = new VerticalPanel();

    public WidgetHolder(WidgetHolderPresenter presenter) {
        this.userPresenter = presenter.getUserPresenter();
        this.taskPresenter = presenter.getTaskPresenter();
        this.chartPresenter = presenter.getChartPresenter();
        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        horizontalPanel.setHeight("100%");
        horizontalPanel.add(initMenuBar());
        horizontalPanel.add(initCenterHolder(userPresenter.getView()));
        RootPanel.get().add(this);
        setSize("100%", "100%");
        add(new HTML("<p></p>"), DockPanel.NORTH);
        add(horizontalPanel, DockPanel.CENTER);
        setCellHorizontalAlignment(horizontalPanel, HasHorizontalAlignment.ALIGN_CENTER);
        setCellVerticalAlignment(horizontalPanel, HasVerticalAlignment.ALIGN_MIDDLE);
        setUpHandlers();
    }

    public void onFirstLoad() {
        initCenterHolder(userPresenter.getView());
    }

    private Widget initMenuBar() {
        panel = new VerticalPanel();
        panel.setWidth("320px");
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

        usersButton = new Button("Рейтинг пользователей");
        usersButton.setType(ButtonType.PRIMARY);
        usersButton.setWidth("200px");
        tasksButton = new Button("Статистика задач");
        tasksButton.setType(ButtonType.PRIMARY);
        tasksButton.setWidth("200px");
        chartButton = new Button("Графики");
        chartButton.setType(ButtonType.PRIMARY);
        chartButton.setWidth("200px");

        panel.add(usersButton);
        panel.add(new HTML("<p></p>"));
        panel.add(tasksButton);
        panel.add(new HTML("<p></p>"));
        panel.add(chartButton);

        return panel;
    }

    private void setUpHandlers() {
        usersButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                initCenterHolder(userPresenter.getView());
            }
        });
        tasksButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                initCenterHolder(taskPresenter.getView());
            }
        });
        chartButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                initCenterHolder(chartPresenter.getView());
            }
        });
    }

    private Widget initCenterHolder(IsWidget widget) {
        centerHolder.clear();
        centerHolder.add(widget);
        centerHolder.setCellVerticalAlignment(widget, HasVerticalAlignment.ALIGN_MIDDLE);
        return centerHolder;
    }
}
