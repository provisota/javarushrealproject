package com.javarush.rating.client.view.charts;

/**
 * @author Alterovych Ilya
 */
public enum ChartType {
    CHART_1(0, "кол-во пользователей на уровне"),
    CHART_2(1, "ср. кол-во попыток на уровень"),
    CHART_3(2, "кол-во пройденных уровней за период"),
    CHART_4(3, "кол-во решённых задач за период");

    private int indexInUIList;
    private String titleInUIList;

    private ChartType(int indexInUIList, String titleInUIList) {
        this.indexInUIList = indexInUIList;
        this.titleInUIList = titleInUIList;
    }

    public static ChartType getBySelectedIndex(int indexInUIList) {
        for (ChartType type : values()) {
            if (type.indexInUIList == indexInUIList) {
                return type;
            }
        }
        throw new UnsupportedOperationException();
    }

    public String getTitleInUIList() {
        return titleInUIList.toLowerCase();
    }
}
