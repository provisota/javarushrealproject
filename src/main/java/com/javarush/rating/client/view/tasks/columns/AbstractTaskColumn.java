package com.javarush.rating.client.view.tasks.columns;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.dom.client.Style;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.javarush.rating.shared.domain.TaskStatsEntity;

/**
 * Базовый абстрактный класс для классов описывающих
 * колонки в таблице (DataGrid) статистики задач.
 * @author Alterovych Ilya
 */
abstract class AbstractTaskColumn extends Column<TaskStatsEntity, String> {
    /**
     * конструктор класса
     * @param dataGrid таблица в которую требуется добавить созданную колонку
     * @param name название колонки (отображается в заголовке)
     * @param title всплывающая подсказка (tooltip) для заголовка
     * @param width ширина колонки
     */
    public AbstractTaskColumn(final DataGrid<TaskStatsEntity> dataGrid,
                              final String name, final String title,
                              final int width) {
        super(new TextCell());

        SafeHtml header = new SafeHtml() {
            private static final long serialVersionUID = -1954064779024228979L;

            @Override
            public String asString() {
                return "<p title=\"" + title + "\"; style=\"margin: 0.5em;text-align:center;\">"
                        + name + "</p>";
            }
        };

        setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        setSortable(true);
        dataGrid.addColumn(this, header);
        dataGrid.setColumnWidth(this, width, Style.Unit.PCT);
    }
}
