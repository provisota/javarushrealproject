package com.javarush.rating.client.view.tasks.comparators;

import com.javarush.rating.shared.domain.TaskStatsEntity;

import java.util.Comparator;

/**
 * @author Alterovych Ilya
 *         Date: 06.02.14
 */
class SolvedComparator implements Comparator<TaskStatsEntity> {
    @Override
    public int compare(TaskStatsEntity o1, TaskStatsEntity o2) {
        Double solved1 = o1.getUsrSolved().doubleValue() /
                (o1.getUsrSolved().doubleValue() +
                        o1.getUsrUnsolved().doubleValue()) * 100;
        Double solved2 = o2.getUsrSolved().doubleValue() /
                (o2.getUsrSolved().doubleValue() +
                        o2.getUsrUnsolved().doubleValue()) * 100;
        return solved1.compareTo(solved2);
    }
}
