package com.javarush.rating.client.view.charts;

import com.google.gwt.visualization.client.ChartArea;
import com.google.gwt.visualization.client.LegendPosition;
import com.google.gwt.visualization.client.visualizations.corechart.AxisOptions;
import com.google.gwt.visualization.client.visualizations.corechart.Options;

/**
 * @author Alterovych Ilya
 */
public class ChartOptions{
    private Options options = null;

    public ChartOptions(String hAxisTitle, String vAxisTitle) {
        options = Options.create();
        options.setWidth(700);
        options.setHeight(473);

        AxisOptions hAxisOptions = AxisOptions.create();
        hAxisOptions.setTitle(hAxisTitle);
        options.setHAxisOptions(hAxisOptions);

        AxisOptions vAxisOptions = AxisOptions.create();
        vAxisOptions.setTitle(vAxisTitle);
        options.setVAxisOptions(vAxisOptions);

        ChartArea chartArea = ChartArea.create();
        chartArea.setTop(40);
        chartArea.setLeft(85);
        chartArea.setWidth("85%");
        chartArea.setHeight("80%");
        options.setChartArea(chartArea);

        options.setLegend(LegendPosition.TOP);
    }

    public Options getOptions() {
        return options;
    }
}
