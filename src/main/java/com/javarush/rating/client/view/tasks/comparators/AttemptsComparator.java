package com.javarush.rating.client.view.tasks.comparators;

import com.javarush.rating.shared.domain.TaskStatsEntity;

import java.util.Comparator;

/**
 * @author Alterovych Ilya
 *         Date: 06.02.14
 */
class AttemptsComparator implements Comparator<TaskStatsEntity> {
    @Override
    public int compare(TaskStatsEntity o1, TaskStatsEntity o2) {
        Double attempt1 = (o1.getAttemptUnsolved().doubleValue() +
                o1.getAttemptSolved().doubleValue()) /
                o1.getUsrSolved().doubleValue();
        Double attempt2 = (o2.getAttemptUnsolved().doubleValue() +
                o2.getAttemptSolved().doubleValue()) /
                o2.getUsrSolved().doubleValue();
        return attempt1.compareTo(attempt2);
    }
}