package com.javarush.rating.client.view.tasks.comparators;

import com.javarush.rating.shared.domain.TaskStatsEntity;

import java.util.Comparator;

/**
 * @author Alterovych Ilya
 *         Date: 07.02.14
 */
class MaxAttemptsComparator implements Comparator<TaskStatsEntity> {
    @Override
    public int compare(TaskStatsEntity o1, TaskStatsEntity o2) {
        return o1.getMaxAttempts().compareTo(o2.getMaxAttempts());
    }
}
