package com.javarush.rating.client.view.tasks;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.CellPreviewEvent;
import com.javarush.rating.client.callback.CommonAsyncCallback;
import com.javarush.rating.client.presenter.TaskPresenter;
import com.javarush.rating.client.view.tasks.columns.ColumnsHolder;
import com.javarush.rating.client.view.tasks.comparators.*;
import com.javarush.rating.shared.domain.TaskStatsEntity;
import org.gwtbootstrap3.client.ui.DataGrid;
import org.gwtbootstrap3.client.ui.ListBox;
import org.gwtbootstrap3.client.ui.Tooltip;
import org.gwtbootstrap3.client.ui.constants.Placement;

import java.util.ArrayList;

/**
 * Author: Alterovych Ilya
 * Date: 29.01.14
 */
public class TasksStatisticView extends Composite {
    private TasksStatisticViewUiBinder ourUiBinder = GWT.create(TasksStatisticViewUiBinder.class);
    private TaskPresenter presenter;
    private ColumnsHolder columnsHolder;
    private ComparatorsHolder comparatorsHolder;
    int level = 1;

    @UiField(provided = true)
    ListBox tasksListBox;
    @UiField(provided = true)
    DataGrid<TaskStatsEntity> dataGrid;

    @UiConstructor
    public TasksStatisticView(TaskPresenter presenter) {
        this.presenter = presenter;
    }

    @SuppressWarnings("unused")
    @UiHandler("tasksListBox")
    public void setTasksListBoxListener(ChangeEvent event) {
        int selectedIndex = tasksListBox.getSelectedIndex();
        String string = tasksListBox.getValue(selectedIndex);
        Integer level = Integer.valueOf(string.substring(string.lastIndexOf(" ") + 1));
        this.level = level;
        presenter.displayTaskStatistic(level);
    }

    public void init() {
        setupDataGrid();
        setupListBox();
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    private void setupDataGrid() {
        dataGrid = new DataGrid<TaskStatsEntity>(100);
        dataGrid.setVisible(true);
        presenter.displayTaskStatistic();

        //Empty table message
        dataGrid.setEmptyTableWidget(new Label("No available data"));

        //add SortHandler
        ColumnSortEvent.ListHandler<TaskStatsEntity> sortHandler =
                new ColumnSortEvent.ListHandler<TaskStatsEntity>(presenter.getModel().getResultList()){
                    @Override
                    public void onColumnSort(ColumnSortEvent event) {
                        setList(presenter.getModel().getResultList());
                        super.onColumnSort(event);
                        presenter.getModel().setResultList((ArrayList<TaskStatsEntity>) getList());
                        refreshTable();
                    }
                };
        dataGrid.addColumnSortHandler(sortHandler);

        //Initialize the variableColumns and add comparators to them
        columnsHolder = new ColumnsHolder(dataGrid);
        comparatorsHolder = new ComparatorsHolder();
        sortHandler.setComparator(columnsHolder.getTaskNameColumn(), comparatorsHolder.getTaskNameComparator());
        sortHandler.setComparator(columnsHolder.getAttemptsColumn(), comparatorsHolder.getAttemptsComparator());
        sortHandler.setComparator(columnsHolder.getMaxAttemptsColumn(), comparatorsHolder.getMaxAttemptsComparator());
        sortHandler.setComparator(columnsHolder.getSolvedColumn(), comparatorsHolder.getSolvedComparator());

        dataGrid.addCellPreviewHandler(new CellPreviewEvent.Handler<TaskStatsEntity>() {
            @Override
            public void onCellPreview(CellPreviewEvent<TaskStatsEntity> event) {
                String selectedRow = event.getValue().getTitle();
                if ("mouseover".equals(event.getNativeEvent().getType())) {
                    dataGrid.getRowElement(event.getIndex()).getCells().getItem(event.getColumn()).setTitle(selectedRow);
                }
            }
        });
    }

    public void refreshTable() {
        refreshTableData();
        dataGrid.redraw();
    }

    private void refreshTableData() {
        ArrayList<TaskStatsEntity> taskList = presenter.getModel().getResultList();
        dataGrid.setRowCount(taskList.size(), true);
        dataGrid.setRowData(0, taskList);
    }

    private void setupListBox() {
        tasksListBox = new ListBox();
        Tooltip tooltip = new Tooltip();
        tooltip.setText("Выберите нужный уровень");
        tooltip.setPlacement(Placement.RIGHT);
        tooltip.add(tasksListBox);

        presenter.getTaskStatsService().findAllLevels(new CommonAsyncCallback<ArrayList<Integer>>() {
            @Override
            public void onSuccess(ArrayList<Integer> result) {
                for (int level : result)
                    tasksListBox.addItem("Уровень " + level);
            }
        });
        tasksListBox.setWidth("50%");
        tasksListBox.setVisibleItemCount(1);
    }

    interface TasksStatisticViewUiBinder extends UiBinder<VerticalPanel, TasksStatisticView> {
    }

}



