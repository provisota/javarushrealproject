package com.javarush.rating.client.view.tasks.columns;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.DataGrid;
import com.javarush.rating.shared.domain.TaskStatsEntity;

/**
 * @author Alterovych Ilya
 *         Date: 08.02.14
 */
class SolvedColumn extends AbstractTaskColumn {

    public SolvedColumn(DataGrid<TaskStatsEntity> dataGrid, String name, String title, int width) {
        super(dataGrid, name, title, width);
    }

    @Override
    public String getValue(TaskStatsEntity taskObject) {
        NumberFormat nf = NumberFormat.getFormat("##########.00");
        if (taskObject.getUsrSolved() == 0)
            return "Будь первым! :)";
        else
            return String.valueOf(nf.format(taskObject.getUsrSolved().doubleValue() /
                    (taskObject.getUsrSolved().doubleValue() +
                            taskObject.getUsrUnsolved().doubleValue()) * 100));
    }
}
