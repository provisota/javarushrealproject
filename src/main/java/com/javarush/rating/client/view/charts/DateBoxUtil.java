package com.javarush.rating.client.view.charts;

import com.google.gwt.event.logical.shared.ShowRangeEvent;
import com.google.gwt.event.logical.shared.ShowRangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import java.util.Date;

/**
 * @author Alterovych Ilya
 */
public abstract class DateBoxUtil {
    static DateTimeFormat dateFormat;

    static void setRangeLimit(final DateBox dateBox, final DateTimeFormat format){
        dateFormat = format;

        final DatePicker endDatePicker = dateBox.getDatePicker();
        final Date first = zeroTime(new Date());
        //число 1356732000000L соответствует 2012-12-29 (дате первой записи в БД)
        first.setTime(1356732000000L);
        endDatePicker.addShowRangeHandler(new ShowRangeHandler<Date>() {
            @Override
            public void onShowRange(ShowRangeEvent<Date> event) {
                Date start = event.getStart();
                Date temp = CalendarUtil.copyDate(start);
                Date end = event.getEnd();
                Date today = today();
                while (temp.before(end)) {
                    if ((temp.after(today) && endDatePicker.isDateVisible(temp)) ||
                            temp.before(first) && endDatePicker.isDateVisible(temp)) {
                        endDatePicker.setTransientEnabledOnDates(false, temp);
                    }
                    CalendarUtil.addDaysToDate(temp, 1);
                }
            }
        });

        dateBox.addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(final ValueChangeEvent<Date> dateValueChangeEvent) {
                if (dateValueChangeEvent.getValue().after(today())) {
                    dateBox.setValue(today(), false);
                }
                if (dateValueChangeEvent.getValue().before(first)) {
                    dateBox.setValue(first, false);
                }
            }
        });
    }

    private static Date today() {
        return zeroTime(new Date());
    }
    /**
     * это нужно чтобы избавиться от временной части, в том числе мс
     */
    private static Date zeroTime(final Date date) {
        return dateFormat.parse(dateFormat.format(date));
    }
}
