package com.javarush.rating.client.view.charts;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Label;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.LabelType;

import java.util.Date;

/**
 * @author Alterovych Ilya
 */
public class ChartDateBox {
    private Date startDate;
    private Date endDate;
    private HorizontalPanel chartDateBoxPanel;
    private final DateBox startDateBox;
    private final DateBox endDateBox;

    public ChartDateBox(final ChartView view) {
        chartDateBoxPanel = new HorizontalPanel();
        chartDateBoxPanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        chartDateBoxPanel.setStyleName("chart-date-box-panel");
        DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd MMMM yyyy");

        VerticalPanel startVerticalPanel = new VerticalPanel();
        startVerticalPanel.setHeight("3em");
        startVerticalPanel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        startVerticalPanel.setVerticalAlignment(HasAlignment.ALIGN_BOTTOM);
        startVerticalPanel.setStyleName("date-box");
        Label startLabel = new Label("начальная дата:");
        startLabel.setType(LabelType.PRIMARY);
        startDateBox = new DateBox();
        startDateBox.setStyleName("font-size-medium");
        startDateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
        startVerticalPanel.add(startLabel);
        startVerticalPanel.add(startDateBox);
        DateBoxUtil.setRangeLimit(startDateBox, dateFormat);

        VerticalPanel endVerticalPanel = new VerticalPanel();
        endVerticalPanel.setHeight("3em");
        endVerticalPanel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        endVerticalPanel.setVerticalAlignment(HasAlignment.ALIGN_BOTTOM);
        endVerticalPanel.setStyleName("date-box");
        Label endLabel = new Label("конечная дата:");
        endLabel.setType(LabelType.PRIMARY);
        endDateBox = new DateBox();
        endDateBox.setStyleName("font-size-medium");
        endDateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
        endVerticalPanel.add(endLabel);
        endVerticalPanel.add(endDateBox);
        DateBoxUtil.setRangeLimit(endDateBox, dateFormat);

        Button confirmButton = new Button("отобразить");
        confirmButton.setType(ButtonType.PRIMARY);
        confirmButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                startDate = startDateBox.getValue();
                endDate = endDateBox.getValue();
                view.getChartPresenter().displayChart(view.getSelectedChartType());
            }
        });

        chartDateBoxPanel.add(startVerticalPanel);
        chartDateBoxPanel.add(endVerticalPanel);
        chartDateBoxPanel.add(confirmButton);
    }

    public void reset(){
        startDate = null;
        startDateBox.setValue(null);
        endDate = null;
        endDateBox.setValue(null);
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public HorizontalPanel getChartDateBoxPanel() {
        return chartDateBoxPanel;
    }
}
