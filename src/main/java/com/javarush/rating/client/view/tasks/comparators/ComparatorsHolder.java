package com.javarush.rating.client.view.tasks.comparators;

/**
 * @author Alterovych Ilya
 *         Date: 13.02.14
 */
public class ComparatorsHolder {
    private TaskNameComparator taskNameComparator;
    private AttemptsComparator attemptsComparator;
    private MaxAttemptsComparator maxAttemptsComparator;
    private SolvedComparator solvedComparator;

    public ComparatorsHolder() {
        taskNameComparator = new TaskNameComparator();
        attemptsComparator = new AttemptsComparator();
        maxAttemptsComparator = new MaxAttemptsComparator();
        solvedComparator = new SolvedComparator();
    }

    public SolvedComparator getSolvedComparator() {
        return solvedComparator;
    }

    public void setSolvedComparator(SolvedComparator solvedComparator) {
        this.solvedComparator = solvedComparator;
    }

    public TaskNameComparator getTaskNameComparator() {
        return taskNameComparator;
    }

    public void setTaskNameComparator(TaskNameComparator taskNameComparator) {
        this.taskNameComparator = taskNameComparator;
    }

    public AttemptsComparator getAttemptsComparator() {
        return attemptsComparator;
    }

    public void setAttemptsComparator(AttemptsComparator attemptsComparator) {
        this.attemptsComparator = attemptsComparator;
    }

    public MaxAttemptsComparator getMaxAttemptsComparator() {
        return maxAttemptsComparator;
    }

    public void setMaxAttemptsComparator(MaxAttemptsComparator maxAttemptsComparator) {
        this.maxAttemptsComparator = maxAttemptsComparator;
    }
}
