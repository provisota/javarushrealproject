package com.javarush.rating.client.view.tasks.columns;

import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.javarush.rating.shared.domain.TaskStatsEntity;

/**
 * @author Alterovych Ilya
 *         Date: 13.02.14
 */
public class ColumnsHolder {
    private TaskNameColumn taskNameColumn;
    private AttemptsColumn attemptsColumn;
    private MaxAttemptsColumn maxAttemptsColumn;
    private SolvedColumn solvedColumn;

    public ColumnsHolder(DataGrid<TaskStatsEntity> dataGrid) {
        taskNameColumn = new TaskNameColumn(dataGrid, "Задача", "Номер задачи", 60);
        taskNameColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
        attemptsColumn = new AttemptsColumn(dataGrid, "Ср. попыток", "среднее количество попыток", 40);
        maxAttemptsColumn = new MaxAttemptsColumn(dataGrid, "Макс. попыток", "максимальное количество попыток", 45);
        solvedColumn = new SolvedColumn(dataGrid, "Решило (%)", "задачу решило (%)", 40);
    }

    public TaskNameColumn getTaskNameColumn() {
        return taskNameColumn;
    }

    public void setTaskNameColumn(TaskNameColumn taskNameColumn) {
        this.taskNameColumn = taskNameColumn;
    }

    public AttemptsColumn getAttemptsColumn() {
        return attemptsColumn;
    }

    public void setAttemptsColumn(AttemptsColumn attemptsColumn) {
        this.attemptsColumn = attemptsColumn;
    }

    public MaxAttemptsColumn getMaxAttemptsColumn() {
        return maxAttemptsColumn;
    }

    public void setMaxAttemptsColumn(MaxAttemptsColumn maxAttemptsColumn) {
        this.maxAttemptsColumn = maxAttemptsColumn;
    }

    public SolvedColumn getSolvedColumn() {
        return solvedColumn;
    }

    public void setSolvedColumn(SolvedColumn solvedColumn) {
        this.solvedColumn = solvedColumn;
    }
}
