package com.javarush.rating.client.view.users;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.RowStyles;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.javarush.rating.client.presenter.UserPresenter;
import com.javarush.rating.client.ratingtype.ProgressRatingType;
import com.javarush.rating.client.ratingtype.TimeRatingType;
import com.javarush.rating.client.resources.IconsResources;
import com.javarush.rating.client.view.CustomPager;
import com.javarush.rating.shared.dto.User;
import com.javarush.rating.shared.helpers.RatingConfig;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.DataGrid;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.ListBox;

import java.util.ArrayList;
import java.util.HashMap;


public class UsersStatisticView extends Composite implements IsWidget {

    private final UserPresenter presenter;

    @UiField
    Button allTimeUsersButton;
    @UiField
    Button monthUsersButton;
    @UiField
    Button weekUsersButton;
    @UiField
    Button clearButton;
    @UiField(provided = true)
    SuggestBox suggestBox;
    @UiField(provided = true)
    ListBox usersListBox;
    @UiField(provided = true)
    DataGrid<User> dataGrid;
    @UiField(provided = true)
    CustomPager pager;

    private UsersStatisticUiBinder ourUiBinder = GWT.create(UsersStatisticUiBinder.class);

    /**
     * HashMap's для хранения первых и последних колонок, которые меняются в DataGrid.
     * Смотреть (UserStatisticColumn)
     */
    private HashMap<ProgressRatingType, UserStatisticColumn> variableLastColumns = new HashMap<>();
    private HashMap<ProgressRatingType, UserStatisticColumn> variableFirstColumns = new HashMap<>();
    private boolean personalRatingData = false;
    private final MultiWordSuggestOracle oracle = new MultiWordSuggestOracle();

    /**
     * Конструктор принимает только презентер, который потом будет управлять логикой виджета.
     *
     * @param presenter смотреть (@UserPresenter)
     */
    @UiConstructor
    public UsersStatisticView(UserPresenter presenter) {
        this.presenter = presenter;
    }

    /**
     * Метод показывает, отображает ли виджет общую статистику или же статистику
     * по определенному юзеру.
     *
     * @return true если отображается статистика по определенному юзеру. В противном случает false;
     */
    public boolean isPersonalRatingDataModeSelected() {
        return personalRatingData;
    }

    /**
     * Метод init() вызывается только при инициализации виджета.
     */
    public void init() {
        setupDataGrid();
        setupUsersListBox();
        setupSuggestBox();
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    @SuppressWarnings("unused")
    @UiHandler("allTimeUsersButton")
    public void setAllTimeUsersButtonListener(ClickEvent event) {
        allTimeUsersButton.setActive(true);
        weekUsersButton.setActive(false);
        monthUsersButton.setActive(false);
        presenter.getModel().setSelectedTimeRatingType(TimeRatingType.TOTAL);
        clearAllSuggestions();
        if (isPersonalRatingDataModeSelected()) {
            pager.erase();
        } else {
            pager.erase();
            presenter.displayUsersStatistic(pager.getPageNumber());
        }
    }

    @SuppressWarnings("unused")
    @UiHandler("weekUsersButton")
    public void setWeekUsersButtonListener(ClickEvent event) {
        weekUsersButton.setActive(true);
        allTimeUsersButton.setActive(false);
        monthUsersButton.setActive(false);
        presenter.getModel().setSelectedTimeRatingType(TimeRatingType.WEEK);
        clearAllSuggestions();
        if (isPersonalRatingDataModeSelected()) {
            pager.erase();
        } else {
            pager.erase();
            presenter.displayUsersStatistic(pager.getPageNumber());
        }
    }

    @SuppressWarnings("unused")
    @UiHandler("monthUsersButton")
    public void setMonthUsersButtonListener(ClickEvent event) {
        monthUsersButton.setActive(true);
        allTimeUsersButton.setActive(false);
        weekUsersButton.setActive(false);
        presenter.getModel().setSelectedTimeRatingType(TimeRatingType.MONTH);
        clearAllSuggestions();
        if (isPersonalRatingDataModeSelected()) {
            pager.erase();
        } else {
            pager.erase();
            presenter.displayUsersStatistic(pager.getPageNumber());
        }
    }

    @SuppressWarnings("unused")
    @UiHandler("usersListBox")
    public void setUsersListBoxListener(ChangeEvent event) {
        ProgressRatingType properColumn = ProgressRatingType.getBySelectedIndex(usersListBox.getSelectedIndex());
        presenter.getModel().setSelectedProgressRatingType(properColumn);
        clearAllSuggestions();
        if (isPersonalRatingDataModeSelected()) {
            pager.erase();
        } else {
            pager.erase();
            presenter.displayUsersStatistic(pager.getPageNumber());
        }
    }

    @SuppressWarnings("unused")
    @UiHandler("clearButton")
    public void onClearButtonClick(ClickEvent event) {
        clearSuggestBox();
    }

    /**
     * Метод вызывается только при показе личной статистики.
     * Метод ищет в какой строке находится DTO объект с искомым ID.
     * В зависимости от установленного в данный момент рейтинга ищется индекс строки (
     * если rating % 10 вернул 0, юзер находится на 10 * pageCount месте.
     * Индекс строки * = найденное место в рейтинге % 10 - 1);
     * Затем DataGrid перерисовывается.
     *
     * @param list список DTO полученных в результате запроса на сервер.
     */
    public void setColorSelection(ArrayList<User> list) {
        int index = -1;
        for (User user : list) {
            if (user.getDisplayName().equals(presenter.getModel().getSelectedUserName())) {
                switch (presenter.getModel().getSelectedProgressRatingType()) {
                    case SOLVED:
                        index = user.getSolvedRating() % RatingConfig.PAGE_SIZE;
                        break;
                    case LEVEL:
                        index = user.getLvlRating() % RatingConfig.PAGE_SIZE;
                        break;
                    case AVERAGE:
                        index = user.getAvgRating() % RatingConfig.PAGE_SIZE;
                        break;
                }
            }
        }
        if (index == 0) {
            index = RatingConfig.PAGE_SIZE;
        }
        final int i = index - 1;
        dataGrid.setRowStyles(new RowStyles<User>() {
            @Override
            public String getStyleNames(User rowObject, int rowIndex) {
                return (rowIndex == i) ? "info" : "default-user-row";
            }
        });
        dataGrid.redraw();
    }

    /**
     * Метод вызывается в режиме показа общей статистики.
     * Снимает все выделения цвета со всех строк в DataGrid,
     * после чего DataGrid перерисовывается.
     */
    public void eraseColorSelection() {
        dataGrid.setRowStyles(new RowStyles<User>() {
            @Override
            public String getStyleNames(User rowObject, int rowIndex) {
                return "default-user-row";
            }
        });
        dataGrid.redraw();
    }

    /**
     * Метод возвращает текущий пейджер для манипуляции с данными.
     *
     * @return смотреть (@CustomPager)
     */
    public CustomPager getPager() {
        return pager;
    }

    /**
     * Инициализация DataGrid'a.
     * Значения по умолчанию, устанавливающиеся для виджета:
     * Отображение 10 строк на странице;
     * Видимость true;
     * Установка SelectionModel, который при клике на строку возвращает DTO (@User),
     * который впоследствии необходим для вызова личного виджета для объекта. Смотреть (@UserWidget)
     * Установка виджета, который будет показан при отсутствии данных в DataGrid'e (setEmptyTableWidget)
     * смотреть (@IconResources)
     * Инициализация колонок для DataGrid'a. Смотреть (@UserStatisticColumn)
     */
    private void setupDataGrid() {
        dataGrid = new DataGrid<>(10);
        dataGrid.setVisible(true);

        pager = new CustomPager(dataGrid, presenter);

        setSelectionModel();

        //Empty table message
        dataGrid.setEmptyTableWidget(new Image(IconsResources.resources.noData()));

        // Initialize the variableLastColumns.
        initTableColumns();
    }

    public void setSelectionModel() {
        final SingleSelectionModel<User> selectionModel = new SingleSelectionModel<>(new ProvidesKey<User>() {
            @Override
            public Object getKey(User item) {
                return item.getId();
            }
        });

        selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                final User user = selectionModel.getSelectedObject();
                presenter.onUserSelectionChange(user);
                selectionModel.setSelected(user, !selectionModel.isSelected(user));
            }
        });

        dataGrid.setSelectionModel(selectionModel);
    }

    /**
     * Инициализация колонок (первых и последних) и добавление меняющихся колонок
     * в variableFirstColumns и variableLastColumns для последующего их использования.
     */
    public void initTableColumns() {

        // variable [Index] column
        UserStatisticColumn solvedRatingColumn = new UserStatisticColumn("Место", 45) {
            @Override
            public String getValue(User userObject) {
                return String.valueOf(userObject.getSolvedRating());
            }
        };

        // variable [Index] column
        UserStatisticColumn levelRatingColumn = new UserStatisticColumn("Место", 45) {
            @Override
            public String getValue(User userObject) {
                return String.valueOf(userObject.getLvlRating());
            }
        };

        // variable [Index] column
        UserStatisticColumn averageRatingColumn = new UserStatisticColumn("Место", 45) {
            @Override
            public String getValue(User userObject) {
                return String.valueOf(userObject.getAvgRating());
            }
        };

        // constant [User name] column
        new UserStatisticColumn("Имя пользователя", 70) {
            @Override
            public String getValue(User userObject) {
                return userObject.getDisplayName();
            }
        }.addToDataGrid(dataGrid, false);

        // variable [Solved tasks] column
        UserStatisticColumn solvedColumn = new UserStatisticColumn("Задач решено", 50) {
            @Override
            public String getValue(User userObject) {
                return userObject.getSolved().toString();
            }
        };

        // variable [Level] column
        UserStatisticColumn levelColumn = new UserStatisticColumn("Уровень", 50) {
            @Override
            public String getValue(User userObject) {
                return userObject.getLevel().toString();
            }
        };

        // variable [Attempt] column
        UserStatisticColumn attemptColumn = new UserStatisticColumn("Попыток на задачу", 50) {
            @Override
            public String getValue(User userObject) {
                return userObject.getAverage().toString();
            }
        };

        //init variable columns
        variableFirstColumns.put(ProgressRatingType.SOLVED, solvedRatingColumn);
        variableFirstColumns.put(ProgressRatingType.LEVEL, levelRatingColumn);
        variableFirstColumns.put(ProgressRatingType.AVERAGE, averageRatingColumn);

        variableLastColumns.put(ProgressRatingType.SOLVED, solvedColumn);
        variableLastColumns.put(ProgressRatingType.LEVEL, levelColumn);
        variableLastColumns.put(ProgressRatingType.AVERAGE, attemptColumn);

        refreshTable();
    }

    /**
     * Метод refreshTable меняет колонки в DataGrid'e если это необходимо,
     * обновляет данные по пользователям и перерисовывает DataGrid;
     */
    public void refreshTable() {
        fixVariableColumnIfNecessary();
        refreshTableData();
        dataGrid.redraw();
    }

    /**
     * Обновление данных в DataGrid. Данные берутся через презентера, который после запроса к серверу
     * сохраняет их в модели, и затем предоставляет доступ к ним. Смотреть (@UserPresenter -> getModel()) (@UserModel)
     */
    private void refreshTableData() {
        ArrayList<User> totalRatingList = presenter.getModel().getResultList();
        dataGrid.setRowCount(totalRatingList.size(), true);
        dataGrid.setRowData(0, totalRatingList);
    }

    /**
     * Изменение колонок в DataGrid; (Происходит при инициализации и при
     * изменении состояния виджета)
     * Первая проверка показывает, проинициализирован ли уже DataGrid.
     * 1. Если нет, то проверка всегда вернет false.
     * В зависимости от выбранного (@ProgressRatingType) из HashMap'ов
     * variableFirstColumns и variableLastColumns получаются необходимые
     * колонки и устанавливаются в DataGrid.
     * 2. Если DataGrid уже проинициализирован, проверка всегда вернет true.
     * Это означает, что состояние виджета поменялось, и необходимо отобразить новые данные.
     * Индексы колонок в DataGrid - 0, 1, 2;
     * Первой удаляется колонка с индексом 0, остальные индексы смещаются, соответственно
     * следующей удаляется последняя колонка с текущим индексом 1;
     * Далее смотреть пункт 1;
     */
    private void fixVariableColumnIfNecessary() {
        if (dataGrid.getColumnCount() > 2) {
            dataGrid.removeColumn(0);
            dataGrid.removeColumn(1);
        }
        ProgressRatingType sortedColumn = presenter.getModel().getSelectedProgressRatingType();
        variableLastColumns.get(sortedColumn).addToDataGrid(dataGrid, true);
        variableFirstColumns.get(sortedColumn).insertToDataGrid(0, dataGrid, true);
    }

    /**
     * Инициализация ListBox'a.
     * Кол-во показываемых строк по умолчанию = 1;
     * Значения для ListBox берутся из enum'a (@ProgressRatingType)
     * Для добавления значений необходимо добавить новый инстанс в (@ProgressRatingType);
     */
    private void setupUsersListBox() {
        usersListBox = new ListBox();
        for (ProgressRatingType type : ProgressRatingType.values()) {
            usersListBox.addItem(type.getTitleInUIList());
        }
        usersListBox.setWidth("430px");
        usersListBox.setVisibleItemCount(1);
    }

    private void setupSuggestBox() {
        suggestBox = new SuggestBox(oracle);

        suggestBox.addKeyPressHandler(new KeyPressHandler() {
            @Override
            public void onKeyPress(KeyPressEvent event) {
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        presenter.fillSuggestions(suggestBox.getText());
                        oracle.addAll(presenter.getModel().getSuggestions());
                    }
                });
            }
        });

        suggestBox.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                resetView();
            }
        });

        suggestBox.addSelectionHandler(new SelectionHandler<SuggestOracle.Suggestion>() {
            @Override
            public void onSelection(SelectionEvent<SuggestOracle.Suggestion> event) {
                presenter.getModel().setSelectedUserName(event.getSelectedItem().getReplacementString());
                presenter.displaySelectedUser();
            }
        });
    }

    private void resetView() {
        if (suggestBox.getText().isEmpty()) {
            personalRatingData = false;
            presenter.displayUsersStatistic(pager.getPageNumber());
            pager.setVisible(true);
        } else {
            personalRatingData = true;
            pager.setVisible(false);
        }
    }

    private void clearSuggestBox() {
        suggestBox.setText("");
        resetView();
    }

    private void clearAllSuggestions() {
        presenter.getModel().clearSuggestions();
        oracle.clear();
        clearSuggestBox();
    }

    interface UsersStatisticUiBinder extends UiBinder<VerticalPanel, UsersStatisticView> {
    }

}