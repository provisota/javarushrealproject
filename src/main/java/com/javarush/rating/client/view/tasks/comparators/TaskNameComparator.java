package com.javarush.rating.client.view.tasks.comparators;

import com.javarush.rating.shared.domain.TaskStatsEntity;

import java.util.Comparator;

/**
 * @author Alterovych Ilya
 *         Date: 06.02.14
 */
public class TaskNameComparator implements Comparator<TaskStatsEntity> {
    @Override
    public int compare(TaskStatsEntity o1, TaskStatsEntity o2) {
        if (o1.getKeyTask().contains("bonus") && o2.getKeyTask().contains("home") ||
                o1.getKeyTask().contains("home") && o2.getKeyTask().contains("bonus"))
            return o2.getKeyTask().compareTo(o1.getKeyTask());
        else
            return o1.getKeyTask().compareTo(o2.getKeyTask());
    }
}
