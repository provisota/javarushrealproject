package com.javarush.rating.client.view.users;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.dom.client.Style;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.javarush.rating.shared.dto.User;

public abstract class UserStatisticColumn extends Column<User, String> {
    private String name;
    private int width;
    private SafeHtml header;

    public UserStatisticColumn(Cell<String> cell, final String name, int width) {
        super(cell);
        this.name = name;
        this.width = width;
        this.header = new SafeHtml() {
            private static final long serialVersionUID = -1204768421603639154L;

            @Override
            public String asString() {
                return "<p style=\"margin: 0.5em;text-align:center;\">"+ name +"</p>";
            }
        };
    }

    public UserStatisticColumn(final String name, int width) {
        this(new TextCell(), name, width);
    }

    /**
     * Метод добавляет колонку на последнюю позицию в DataGrid
     * @param dataGrid виджет, содержащий колонку
     * @param centralizedHeader если значение установлено в true, то header и содержимое колонки
     *                          центрируется, в противном случае устанавливается стандартное
     *                          форматирование по левому краю.
     */
    public void addToDataGrid(DataGrid<User> dataGrid, boolean centralizedHeader) {
        insertToDataGrid(dataGrid.getColumnCount(), dataGrid, centralizedHeader);
    }

    /**
     * Метод добавляет колонку на указанную позицию в DataGrid
     * @param position индекс колонки для добавления
     * @param dataGrid виджет, содержащий колонку
     * @param centralizedHeader если значение установлено в true, то header и содержимое колонки
     *                          центрируется, в противном случае устанавливается стандартное
     *                          форматирование по левому краю.
     */
    public void insertToDataGrid(int position, DataGrid<User> dataGrid, boolean centralizedHeader) {
        if (centralizedHeader) {
            dataGrid.insertColumn(position, this, header);
            setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        } else {
            dataGrid.insertColumn(position, this, name);
        }
        dataGrid.setColumnWidth(this, width, Style.Unit.PCT);
    }

}
