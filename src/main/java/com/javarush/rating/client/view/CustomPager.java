package com.javarush.rating.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.javarush.rating.client.presenter.UserPresenter;
import com.javarush.rating.shared.dto.User;
import com.javarush.rating.shared.helpers.RatingConfig;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.ButtonToolBar;


public class CustomPager extends HorizontalPanel {
    interface CustomPagerUiBinder extends UiBinder<Widget, CustomPager> {
    }
    private static CustomPagerUiBinder ourUiBinder = GWT.create(CustomPagerUiBinder.class);

    private UserPresenter presenter;
    private HasData<User> display;
    private int pageNumber = 0;
    private long pageCount = 0;

    @UiConstructor
    public CustomPager(HasData<User> display, UserPresenter presenter) {
        this.presenter = presenter;
        this.display = display;
        ourUiBinder.createAndBindUi(this);
        this.add(pagerHolder);
        prevButton.setEnabled(false);
    }

    public void setLabelText() {
        pageNumberButton.setText(String.valueOf(pageNumber + 1));
        pageCountButton.setText(String.valueOf(pageCount));
        checkButtonsAvailability();
    }

    public void setLabelText(long rowCount) {
        pageCount = rowCount / RatingConfig.PAGE_SIZE;
        if (rowCount % 10 != 0) {
            pageCount++;
        }
        setLabelText();
    }

    @UiField
    ButtonToolBar pagerHolder;
    @UiField
    Button nextButton;
    @UiField
    Button prevButton;
    @UiField
    Button pageNumberButton;
    @UiField
    Button pageCountButton;

    @SuppressWarnings("unused")
    @UiHandler("nextButton")
    public void nextPage(ClickEvent event) {
        if (getDisplay().getRowCount() == RatingConfig.PAGE_SIZE) {
            pageNumber++;
            presenter.displayUsersStatistic(pageNumber);
            setLabelText();
        }
        checkButtonsAvailability();
    }

    @SuppressWarnings("unused")
    @UiHandler("prevButton")
    public void previousPage(ClickEvent event) {
        if (pageNumber > 0) {
            pageNumber--;
            presenter.displayUsersStatistic(pageNumber);
            setLabelText();
        }
        checkButtonsAvailability();
    }

    public HasData<User> getDisplay() {
        return display;
    }

    /**
     * Cleans pager PageCounter;
     */
    public void erase() {
        pageNumber = 0;
        setLabelText();
        checkButtonsAvailability();
    }

    /**
     * @return актуальный номер страницы для обновления HasData<>;
     * Смотреть (@UsersStatisticView)
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     *  Проверяем кнопки ДАЛЬШЕ и НАЗАД. Если открыта 1 страница, кнопка НАЗАД неактивна,
     *  Если достигнут конец списка кнопка ДАЛЬШЕ неактивна.
     */
    private void checkButtonsAvailability() {
        nextButton.setEnabled(pageNumber != pageCount - 1);

        prevButton.setEnabled(pageNumber != 0);
    }
}