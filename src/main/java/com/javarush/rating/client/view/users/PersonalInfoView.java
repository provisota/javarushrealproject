package com.javarush.rating.client.view.users;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.javarush.rating.client.model.UserModel;
import com.javarush.rating.shared.dto.User;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.Modal;


public class PersonalInfoView {

    interface PersonalInfoViewUiBinder extends UiBinder<Widget, PersonalInfoView> {
    }
    private static PersonalInfoViewUiBinder ourUiBinder = GWT.create(PersonalInfoViewUiBinder.class);

    private UserModel model;

    public PersonalInfoView(UserModel model) {
        this.model = model;
        ourUiBinder.createAndBindUi(this);
    }

    @UiField
    Modal modalBox;
    @UiField
    Image photo;
    @UiField
    Heading name;
    @UiField
    Heading city;
    @UiField
    Heading level;
    @UiField
    Heading solvedTasks;
    @UiField
    Heading attempts;
    @UiField
    Heading hardestTask;
    @UiField
    Heading createdTime;

    public void showSelectedUser() {
        User selectedUser = model.getSelectedUserRow();
        if (selectedUser == null) {
            return;
        }

        photo.setSize("100px", "100px");
        photo.setUrl(selectedUser.getPictureUrl());
        name.setText("Имя: " + selectedUser.getDisplayName());
        city.setText(selectedUser.getCity() == null ? "Город: " + "—" : "Город: " + selectedUser.getCity());
        level.setText("Уровень: " + selectedUser.getLevel());
        solvedTasks.setText("Решенных задач: " + selectedUser.getSolved());
        attempts.setText("Среднее кол-во попыток: " + selectedUser.getAverage());
        hardestTask.setText("Самая трудная задача: " + selectedUser.getHardestTask());
        createdTime.setText("Зарегистрирован: " + selectedUser.getRegistered().toString().split(" ")[0]);

        modalBox.show();
    }

}
