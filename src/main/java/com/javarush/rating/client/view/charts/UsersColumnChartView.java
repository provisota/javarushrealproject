// TODO: оставлено для следующей версии
/*
package com.javarush.rating.client.view.charts;

import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.visualization.client.AbstractDataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.visualizations.corechart.ColumnChart;
import com.google.gwt.visualization.client.visualizations.corechart.Options;
import com.javarush.rating.client.presenter.UserPresenter;
import com.javarush.rating.client.ratingtype.ProgressRatingType;
import com.javarush.rating.shared.dto.User;

import java.util.ArrayList;
import java.util.HashMap;

public class UsersColumnChartView extends SimplePanel {

    private UserPresenter presenter;
    private Options options;
    ColumnChart columnChart;
    HashMap<ProgressRatingType, DataTable> variableTables = new HashMap<ProgressRatingType, DataTable>();


    public UsersColumnChartView(UserPresenter presenter, Panel panel) {
        setVisible(true);
        setSize("100%", "100%");
        this.presenter = presenter;
        //presenter.setColumnChartView(this);
        columnChart = new ColumnChart(createTable(), getOptions());
        columnChart.setVisible(true);
        setWidget(columnChart);
        panel.add(this);
    }

    private Options getOptions() {
        if (options == null) {
            options = Options.create();
            options.setWidth(600);
            options.setHeight(400);

        }
        options.setTitle("Статистика " + presenter.getModel().getSelectedProgressRatingType().getTitleInUIList());
        return options;
    }

    public void draw() {
        columnChart.draw(createTable(), getOptions());
    }

    private AbstractDataTable createTable() {
        initTable();
        DataTable table = variableTables.get(presenter.getModel().getSelectedProgressRatingType());
        if (presenter.getModel().getSelectedProgressRatingType() == ProgressRatingType.SOLVED) {
            initSolvedData(table);
        } else if (presenter.getModel().getSelectedProgressRatingType() == ProgressRatingType.LEVEL) {
            initLevelData(table);
        } else if (presenter.getModel().getSelectedProgressRatingType() == ProgressRatingType.AVERAGE) {
            initAverageData(table);
        }

        return table;
    }

    private void initTable() {
        if (variableTables.isEmpty()) {
            DataTable solvedTable = DataTable.create();
            DataTable.create();
            solvedTable.addColumn(ColumnType.STRING);
            solvedTable.addColumn(ColumnType.NUMBER);
            solvedTable.setColumnLabel(1, "Кол-во");
            solvedTable.addRows(SolvedStatistic.values().length);

            DataTable levelTable = DataTable.create();
            DataTable.create();
            levelTable.addColumn(ColumnType.STRING);
            levelTable.addColumn(ColumnType.NUMBER);
            levelTable.setColumnLabel(1, "Кол-во");
            levelTable.addRows(LevelStatistic.values().length);

            DataTable averageTable = DataTable.create();
            DataTable.create();
            averageTable.addColumn(ColumnType.STRING);
            averageTable.addColumn(ColumnType.NUMBER);
            averageTable.setColumnLabel(1, "Кол-во");
            averageTable.addRows(AverageStatistic.values().length);

            variableTables.put(ProgressRatingType.SOLVED, solvedTable);
            variableTables.put(ProgressRatingType.LEVEL, levelTable);
            variableTables.put(ProgressRatingType.AVERAGE, averageTable);
        }
    }

    private void initSolvedData(DataTable table) {
        SolvedStatistic.cleanValues();

        ArrayList<User> list = presenter.getModel().getResultList();
        for (User user : list) {
            SolvedStatistic.checkValues(user);
        }

        for (int i = 0; i < SolvedStatistic.values().length; i++) {
            table.setValue(i, 0, SolvedStatistic.values()[i].getTitle());
            table.setValue(i, 1, SolvedStatistic.values()[i].getCount());
        }
    }

    private void initLevelData(DataTable table) {
        LevelStatistic.cleanValues();

        ArrayList<User> list = presenter.getModel().getResultList();
        for (User totalRating : list) {
            LevelStatistic.checkValues(totalRating);
        }

        for (int i = 0; i < LevelStatistic.values().length; i++) {
            table.setValue(i, 0, LevelStatistic.values()[i].getTitle());
            table.setValue(i, 1, LevelStatistic.values()[i].getCount());
        }
    }

    private void initAverageData(DataTable table) {
        AverageStatistic.cleanValues();

        ArrayList<User> list = presenter.getModel().getResultList();
        for (User totalRating : list) {
            AverageStatistic.checkValues(totalRating);
        }

        for (int i = 0; i < AverageStatistic.values().length; i++) {
            table.setValue(i, 0, AverageStatistic.values()[i].getTitle());
            table.setValue(i, 1, AverageStatistic.values()[i].getCount());
        }
    }

    private enum SolvedStatistic {
        MORE_THEN_500(0, 500, "Больше 500 задач решено"),
        MORE_THEN_600(0, 600, "Больше 600 задач решено"),
        MORE_THEN_700(0, 700, "Больше 700 задач решено"),
        MORE_THEN_800(0, 800, "Больше 800 задач решено"),
        MORE_THEN_900(0, 900, "Больше 900 задач решено"),
        MORE_THEN_1000(0, 1000, "Больше 1000 задач решено");

        private int boundaryValue;
        private int count;
        private String title;

        private SolvedStatistic(int count, int boundaryValue, String title) {
            this.count = count;
            this.title = title;
            this.boundaryValue = boundaryValue;
        }

        public int getCount() {
            return count;
        }
        public String getTitle() {
            return title;
        }

        public void increment() {
            count++;
        }

        public static void checkValues(User ratingObject) {
            for (int i = SolvedStatistic.values().length - 1; i >= 0; i--) {
                if (ratingObject.getSolved() >= SolvedStatistic.values()[i].boundaryValue) {
                    SolvedStatistic.values()[i].increment();
                    break;
                }
            }
        }

        public static void cleanValues() {
            for (SolvedStatistic solvedStatistic : SolvedStatistic.values()) {
                solvedStatistic.count = 0;
            }
        }
    }

    private enum LevelStatistic {
        MORE_THEN_20_LVL(0, 20, "20+ Уровень"),
        MORE_THEN_25_LVL(0, 25, "25+ Уровень"),
        MORE_THEN_30_LVL(0, 30, "30+ Уровень"),
        MORE_THEN_35_LVL(0, 35, "35+ Уровень");

        private int boundaryValue;
        private int count;
        private String title;

        private LevelStatistic(int count, int boundaryValue, String title) {
            this.count = count;
            this.title = title;
            this.boundaryValue = boundaryValue;
        }

        public int getCount() {
            return count;
        }
        public String getTitle() {
            return title;
        }

        public void increment() {
            count++;
        }

        public static void checkValues(User ratingObject) {
            for (int i = LevelStatistic.values().length - 1; i >= 0; i--) {
                if (ratingObject.getLevel() >= LevelStatistic.values()[i].boundaryValue) {
                    LevelStatistic.values()[i].increment();
                    break;
                }
            }
        }

        public static void cleanValues() {
            for (LevelStatistic levelStatistic : LevelStatistic.values()) {
                levelStatistic.count = 0;
            }
        }
    }

    private enum AverageStatistic {
        LESS_THEN_3(0, 3, "Менее 3 попыток на задачу"),
        LESS_THEN_5(0, 5, "Менее 5 попыток на задачу"),
        LESS_THEN_10(0, 10, "Менее 10 попыток на задачу");

        private int boundaryValue;
        private int count;
        private String title;

        private AverageStatistic(int count, int boundaryValue, String title) {
            this.count = count;
            this.title = title;
            this.boundaryValue = boundaryValue;
        }

        public int getCount() {
            return count;
        }
        public String getTitle() {
            return title;
        }

        public void increment() {
            count++;
        }

        public static void checkValues(User ratingObject) {
            for (AverageStatistic averageStatistic : AverageStatistic.values()) {
                if (ratingObject.getAverage().intValue() < averageStatistic.boundaryValue) {
                    averageStatistic.increment();
                    break;
                }
            }
        }

        public static void cleanValues() {
            for (AverageStatistic averageStatistic : AverageStatistic.values()) {
                averageStatistic.count = 0;
            }
        }
    }
}*/
