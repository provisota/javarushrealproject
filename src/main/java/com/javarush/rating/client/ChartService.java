package com.javarush.rating.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.javarush.rating.shared.dto.ActionByDateChartBean;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Alterovych Ilya
 */
@RemoteServiceRelativePath("springGwtServices/chartService")
public interface ChartService extends RemoteService {

    ArrayList<Integer[]> findCountUsersOnLevels();
    ArrayList<Integer[]> findAvgAttemptsOnLevels();
    ArrayList<ActionByDateChartBean> findActionCountByDate(Integer actionType, Date startDate, Date endDate);
}
