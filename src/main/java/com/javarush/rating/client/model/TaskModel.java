package com.javarush.rating.client.model;


import com.javarush.rating.shared.domain.TaskStatsEntity;

import java.util.ArrayList;

/**
 * пассивная модель, используется презентером TaskPresenter
 * в качестве источника данных для отображения статистики по задачам.
 * @author Alterovych Ilya
 */

public class TaskModel {
    /**
     * результат запроса в БД (список "сущностей TaskStatsEntity")
     */
    private ArrayList<TaskStatsEntity> resultList;

    /**
     * class constructor
     */
    public TaskModel() {
        resultList = new ArrayList<TaskStatsEntity>();
    }

    /**
     * resultList getter
     * @param resultList результат запроса в БД
     */
    public final void setResultList(final ArrayList<TaskStatsEntity> resultList) {
        this.resultList = resultList;
    }

    /**
     * resultList setter
     * @return resultList - результат запроса в БД
     */
    public final ArrayList<TaskStatsEntity> getResultList() {
        return resultList;
    }
}
