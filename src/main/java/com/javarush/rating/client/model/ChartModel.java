package com.javarush.rating.client.model;

import com.javarush.rating.shared.dto.ActionByDateChartBean;

import java.util.ArrayList;

/**
 * @author Alterovych Ilya
 * пассивная модель, используется презентером ChartPresenter
 * в качестве источника данных для отображения графиков.
 */
public class ChartModel {
    /**
     * результат запроса в БД по 1 и 2 графикам
     */
    private ArrayList<Integer[]> resultList;

    /**
     * результат запроса в БД по 3 и 4 графикам
     */
    private ArrayList<ActionByDateChartBean> resultBeanList;

    /**
     * class constructor
     */
    public ChartModel() {
        resultList = new ArrayList<Integer[]>();
    }

    /**
     * resultList getter
     * @param resultList результат запроса в БД по 1 и 2 графикам
     */
    public final void setResultList(final ArrayList<Integer[]> resultList) {
        this.resultList = resultList;
    }

    /**
     * resultList setter
     * @return resultList - результат запроса в БД по 1 и 2 графикам
     */
    public final ArrayList<Integer[]> getResultList() {
        return resultList;
    }

    /**
     * resultBeanList getter
     * @return resultBeanList результат запроса в БД по 3 и 4 графикам
     */
    public final ArrayList<ActionByDateChartBean> getResultBeanList() {
        return resultBeanList;
    }

    /**
     * resultBeanList setter
     * @return resultBeanList результат запроса в БД по 3 и 4 графикам
     */
    public final void setResultBeanList(ArrayList<ActionByDateChartBean> resultBeanList) {
        this.resultBeanList = resultBeanList;
    }
}
