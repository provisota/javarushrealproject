package com.javarush.rating.client.model;

import com.javarush.rating.client.ratingtype.ProgressRatingType;
import com.javarush.rating.client.ratingtype.SortRatingType;
import com.javarush.rating.client.ratingtype.TimeRatingType;
import com.javarush.rating.shared.dto.User;

import java.util.ArrayList;

public class UserModel {
    private final ArrayList<String> suggestions = new ArrayList<>();
    private ProgressRatingType selectedProgressRatingType = ProgressRatingType.SOLVED;
    private TimeRatingType selectedTimeRatingType = TimeRatingType.TOTAL;
    private ArrayList<User> resultList;
    private User selectedUserRow;
    private String selectedUserName;

    public UserModel() {
        this.resultList = new ArrayList<>();
    }

    public ArrayList<String> getSuggestions() {
        return suggestions;
    }

    public ProgressRatingType getSelectedProgressRatingType() {
        return selectedProgressRatingType;
    }

    public void setSelectedProgressRatingType(ProgressRatingType selectedProgressRatingType) {
        this.selectedProgressRatingType = selectedProgressRatingType;
    }

    public SortRatingType getSelectedSortProperty() {
        switch (selectedProgressRatingType) {
            case SOLVED:
                return SortRatingType.SOLVED;
            case LEVEL:
                return SortRatingType.LEVEL;
            case AVERAGE:
                return SortRatingType.AVERAGE;
            default:
                return SortRatingType.SOLVED;
        }
    }

    public ArrayList<User> getResultList() {
        return resultList;
    }

    public void setResultList(ArrayList<User> resultList) {
        this.resultList = resultList;
    }

    public User getSelectedUserRow() {
        return selectedUserRow;
    }

    public void setSelectedUserRow(User selectedUserRow) {
        this.selectedUserRow = selectedUserRow;
    }

    public String getSelectedUserName() {
        return selectedUserName;
    }

    public void setSelectedUserName(String selectedUserName) {
        this.selectedUserName = selectedUserName;
    }

    public TimeRatingType getSelectedTimeRatingType() {
        return selectedTimeRatingType;
    }

    public void setSelectedTimeRatingType(TimeRatingType selectedTimeRatingType) {
        this.selectedTimeRatingType = selectedTimeRatingType;
    }

    public void updateSuggestions(final ArrayList<String> result) {
        clearSuggestions();
        suggestions.addAll(result);
    }

    public void clearSuggestions() {
        if (!suggestions.isEmpty()) {
            suggestions.clear();
        }
    }
}
