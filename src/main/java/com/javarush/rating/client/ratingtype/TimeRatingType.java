package com.javarush.rating.client.ratingtype;

public enum TimeRatingType {
    TOTAL,
    MONTH,
    WEEK
}
