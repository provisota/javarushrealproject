package com.javarush.rating.client.ratingtype;

public enum SortRatingType {

    SOLVED, LEVEL, AVERAGE;

    @Override
    public String toString() {
        switch (this) {
            case SOLVED:
                return "solvedRating";
            case LEVEL:
                return "lvlRating";
            case AVERAGE:
                return "avgRating";
            default:
                return "Unspecified";
        }
    }
}
