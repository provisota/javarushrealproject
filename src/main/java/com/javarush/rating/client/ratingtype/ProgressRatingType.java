package com.javarush.rating.client.ratingtype;

public enum ProgressRatingType {
    SOLVED(0, "по количеству решенных задач"),
    LEVEL(1, "по уровню"),
    AVERAGE(2, "по среднему количеству попыток");

    private int indexInUIList;
    private String titleInUIList;

    private ProgressRatingType(int indexInUIList, String titleInUIList) {
        this.indexInUIList = indexInUIList;
        this.titleInUIList = titleInUIList;
    }

    public static ProgressRatingType getBySelectedIndex(int indexInUIList) {
        for (ProgressRatingType type : values()) {
            if (type.indexInUIList == indexInUIList) {
                return type;
            }
        }
        throw new UnsupportedOperationException();
    }

    public String getTitleInUIList() {
        return titleInUIList.toLowerCase();
    }
}

