package com.javarush.rating.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.javarush.rating.shared.dto.ActionByDateChartBean;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Alterovych Ilya
 */
public interface ChartServiceAsync {

    void findCountUsersOnLevels(AsyncCallback<ArrayList<Integer[]>> async);

    void findAvgAttemptsOnLevels(AsyncCallback<ArrayList<Integer[]>> async);

    void findActionCountByDate(Integer actionType, Date startDate, Date endDate, AsyncCallback<ArrayList<ActionByDateChartBean>> async);
}
