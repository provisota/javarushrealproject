package com.javarush.rating.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface IconsResources extends ClientBundle {

    /**
     * The images consumed by ClientBundle into ImageResources are expected
     * to be in the source tree, not in the external web resources
     * (i.e. in target/ or webapp/);
     */

    @Source("icons/green_star.jpg")
    ImageResource greenStar();

    @Source("icons/blue_star.jpg")
    ImageResource blueStar();

    @Source("icons/gold_star.jpg")
    ImageResource goldStar();

    @Source("icons/helmet.jpg")
    ImageResource helmet();

    @Source("icons/loading_image.jpg")
    ImageResource screenButton();

    @Source("icons/no_available_data.jpg")
    ImageResource noData();

    public static IconsResources resources = GWT.create(IconsResources.class);

}