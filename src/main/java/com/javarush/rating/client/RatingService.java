package com.javarush.rating.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.javarush.rating.client.ratingtype.SortRatingType;
import com.javarush.rating.client.ratingtype.TimeRatingType;
import com.javarush.rating.shared.dto.User;

import java.util.ArrayList;

/**
 * Author: Timur
 * Date: 2/2/14
 */
@RemoteServiceRelativePath("springGwtServices/ratingService")
public interface RatingService extends RemoteService {

    ArrayList<User> findAllByPage(TimeRatingType ratingType, int page, SortRatingType sortRatingType);

    ArrayList<String> findSuggestionsByName(String startsWith, TimeRatingType timeRatingType);

    ArrayList<User> findPageByName(String name, SortRatingType sortRatingType, TimeRatingType timeRatingType);

    long getTotalPages(TimeRatingType ratingType);
}
