package com.javarush.rating.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.javarush.rating.client.model.ChartModel;
import com.javarush.rating.client.model.TaskModel;
import com.javarush.rating.client.model.UserModel;
import com.javarush.rating.client.presenter.ChartPresenter;
import com.javarush.rating.client.presenter.TaskPresenter;
import com.javarush.rating.client.presenter.UserPresenter;
import com.javarush.rating.client.presenter.WidgetHolderPresenter;

/**
 * Author: Timur
 * Date: 1/5/14
 */
public class RatingApplication implements EntryPoint {
    @Override
    public void onModuleLoad() {
        RatingServiceAsync ratingService = GWT.create(RatingService.class);
        UserModel userModel = new UserModel();
        UserPresenter presenter = new UserPresenter(ratingService, userModel);

        TaskStatsServiceAsync taskStatsService = GWT.create(TaskStatsService.class);
        TaskModel taskModel = new TaskModel();
        TaskPresenter tasksPresenter = new TaskPresenter(taskStatsService, taskModel);

        ChartServiceAsync chartService = GWT.create(ChartService.class);
        ChartModel chartModel = new ChartModel();
        ChartPresenter chartPresenter = new ChartPresenter(chartService, chartModel);
        //Тут остольное парам-пам-пам

        WidgetHolderPresenter mainPresenter = new WidgetHolderPresenter(presenter, tasksPresenter, chartPresenter);
        mainPresenter.loadProject();
    }

    public static void displayErrorToUser(Throwable caught) {
        Window.alert("ERROR: " + caught.getMessage());
    }
}
