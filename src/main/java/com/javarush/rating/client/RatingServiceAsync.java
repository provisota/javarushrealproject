package com.javarush.rating.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.javarush.rating.client.ratingtype.SortRatingType;
import com.javarush.rating.client.ratingtype.TimeRatingType;
import com.javarush.rating.shared.dto.User;

import java.util.ArrayList;

public interface RatingServiceAsync {

    void findAllByPage(TimeRatingType ratingType, int page, SortRatingType sortRatingType, AsyncCallback<ArrayList<User>> async);

    void findSuggestionsByName(String startsWith, TimeRatingType timeRatingType, AsyncCallback<ArrayList<String>> async);

    void findPageByName(String name, SortRatingType sortRatingType, TimeRatingType timeRatingType, AsyncCallback<ArrayList<User>> async);

    void getTotalPages(TimeRatingType ratingType, AsyncCallback<Long> async);
}
