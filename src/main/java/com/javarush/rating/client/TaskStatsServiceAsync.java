package com.javarush.rating.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.javarush.rating.shared.domain.TaskStatsEntity;

import java.util.ArrayList;

/**
 * Date: 29.01.14
 *
 * @author Sergey Kandalintsev
 */
public interface TaskStatsServiceAsync {
    void findByLevel(int level, AsyncCallback<ArrayList<TaskStatsEntity>> async);
    void findAllLevels(AsyncCallback<ArrayList<Integer>> async);
}
