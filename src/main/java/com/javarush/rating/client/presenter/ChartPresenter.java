package com.javarush.rating.client.presenter;

import com.javarush.rating.client.ChartServiceAsync;
import com.javarush.rating.client.callback.CommonAsyncCallback;
import com.javarush.rating.client.model.ChartModel;
import com.javarush.rating.client.view.charts.ChartType;
import com.javarush.rating.client.view.charts.ChartView;
import com.javarush.rating.shared.dto.ActionByDateChartBean;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Alterovych Ilya
 */
public class ChartPresenter {
    private ChartServiceAsync chartService;
    private ChartModel model;
    private ChartView view;

    public ChartPresenter(ChartServiceAsync chartService, ChartModel model) {
        this.chartService = chartService;
        this.model = model;
    }


    public void displayChart() {
        displayChart(ChartType.CHART_1);
    }

    public void displayChart(final ChartType chartType) {
        if (chartType == ChartType.CHART_1) {
            chartService.findCountUsersOnLevels(new CommonAsyncCallback<ArrayList<Integer[]>>() {
                @Override
                public void onSuccess(ArrayList<Integer[]> result) {
                    model.setResultList(result);
                    updateData(chartType);
                }
            });
        }

        if (chartType == ChartType.CHART_2) {
            chartService.findAvgAttemptsOnLevels(new CommonAsyncCallback<ArrayList<Integer[]>>() {
                @Override
                public void onSuccess(ArrayList<Integer[]> result) {
                    model.setResultList(result);
                    updateData(chartType);
                }
            });
        }

        if (chartType == ChartType.CHART_3 || chartType == ChartType.CHART_4) {
            Date startDate = view.getChartDateBox().getStartDate();
            Date endDate = view.getChartDateBox().getEndDate();
            if (startDate == null || endDate == null) {
                model.setResultBeanList(new ArrayList<ActionByDateChartBean>());
                updateData(chartType);
                return;
            }
            Integer actionType = 0;
            ChartType selectedChartType = view.getSelectedChartType();
            if (selectedChartType == ChartType.CHART_3)
                actionType = 6;
            if (selectedChartType == ChartType.CHART_4)
                actionType = 4;
            chartService.findActionCountByDate(actionType, startDate, endDate,
                    new CommonAsyncCallback<ArrayList<ActionByDateChartBean>>() {
                        @Override
                        public void onSuccess(ArrayList<ActionByDateChartBean> result) {
                            model.setResultBeanList(result);
                            updateData(chartType);
                        }
                    }
            );
        }
    }

    private void updateData(ChartType chartType) {
        if (view == null) {
            //create and init view
            view = new ChartView(ChartPresenter.this);
            view.init();
        } else {
            //update view
            view.refreshTable(chartType);
        }
    }

    public ChartModel getModel() {
        return model;
    }

    public ChartView getView() {
        return view;
    }
}
