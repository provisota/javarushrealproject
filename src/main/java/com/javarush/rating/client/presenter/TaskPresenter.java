package com.javarush.rating.client.presenter;

import com.javarush.rating.client.TaskStatsServiceAsync;
import com.javarush.rating.client.callback.CommonAsyncCallback;
import com.javarush.rating.client.model.TaskModel;
import com.javarush.rating.client.view.tasks.TasksStatisticView;
import com.javarush.rating.client.view.tasks.comparators.TaskNameComparator;
import com.javarush.rating.shared.domain.TaskStatsEntity;
import java.util.ArrayList;
import java.util.Collections;

/**
 * пресентер статистики задач, извлекает данные из БД,
 * помещает их в хранилище (TaskModel)
 * и даёт команду представлению (TaskView) отобразить их.
 *
 * @author : Alterovych Ilya
 */

public class TaskPresenter {
    /**
     * ссылка на экземпляр класса сервиса используемого для
     * получения данных по задачам из БД
     */
    private TaskStatsServiceAsync taskStatsService;
    /**
     * ссылка на экземпляр класса модели задач
     */
    private TaskModel model;
    /**
     * ссылка на экземпляр класса представления задач
     */
    private TasksStatisticView view;

    /**
     * конструктор класса
     *
     * @param taskStatsService ссылка на экземпляр класса сервиса
     *                         используемого для получения данных по задачам из БД
     * @param model            ссылка на экземпляр класса модели задач
     */
    public TaskPresenter(final TaskStatsServiceAsync taskStatsService,
                         final TaskModel model) {
        this.taskStatsService = taskStatsService;
        this.model = model;
    }

    /**
     *
     */
    public final void displayTaskStatistic() {
        displayTaskStatistic(1);
    }

    /**
     * Главный метод класса. Выполняет всю основную работу, а именно:
     * извлекает данные из БД, помещает их в хранилище (TaskModel)
     * и даёт команду представлению (TaskView) отобразить их.
     *
     * @param level уровень по которому требуется извлечь данные из БД.
     */
    public final void displayTaskStatistic(final int level) {
        taskStatsService.findByLevel(level, new CommonAsyncCallback<ArrayList<TaskStatsEntity>>() {
            @Override
            public void onSuccess(final ArrayList<TaskStatsEntity> result) {
                Collections.sort(result, new TaskNameComparator());
                model.setResultList(result);
                if (view == null) {
                    //create and init view
                    view = new TasksStatisticView(TaskPresenter.this);
                    view.init();
                } else {
                    //update view
                    view.refreshTable();
                }
            }
        });
    }

    /**
     * taskStatsService getter
     *
     * @return экземпляр класса сервиса
     * используемого для получения данных по задачам из БД
     */
    public final TaskStatsServiceAsync getTaskStatsService() {
        return taskStatsService;
    }

    /**
     * taskModel getter
     *
     * @return экземпляр класса модели задач
     */
    public final TaskModel getModel() {
        return model;
    }

    /**
     * view getter
     * @return экземпляр класса представления задач
     */
    public final TasksStatisticView getView() {
        return view;
    }
}
