package com.javarush.rating.client.presenter;

import com.javarush.rating.client.RatingServiceAsync;
import com.javarush.rating.client.callback.CommonAsyncCallback;
import com.javarush.rating.client.model.UserModel;
import com.javarush.rating.client.view.users.PersonalInfoView;
import com.javarush.rating.client.view.users.UsersStatisticView;
import com.javarush.rating.shared.dto.User;
import com.javarush.rating.shared.helpers.RatingConfig;

import java.util.ArrayList;

public class UserPresenter {

    private final RatingServiceAsync ratingService;
    private final UserModel model;
    private UsersStatisticView view;

    public UserPresenter(RatingServiceAsync ratingService, UserModel model) {
        this.ratingService = ratingService;
        this.model = model;
    }

    public void displayUsersStatistic() {
        displayUsersStatistic(0);
    }

    public void displayUsersStatistic(int page) {
        ratingService.findAllByPage(getModel().getSelectedTimeRatingType(),
                page, model.getSelectedSortProperty(), new CommonAsyncCallback<ArrayList<User>>() {
                    @Override
                    public void onSuccess(ArrayList<User> result) {
                        updateData(result);
                    }
                }
        );
    }

    private void updateData(ArrayList<User> list) {
        model.setResultList(list);
        if (view == null) {
            view = new UsersStatisticView(UserPresenter.this);
            view.init();
            updateTotalRowCount();
            hidePagerIfNecessary(list);
            updateColorSelection(list);
        } else {
            //update view
            view.refreshTable();
            updateTotalRowCount();
            hidePagerIfNecessary(list);
            updateColorSelection(list);
        }
    }

    /**
     * Метод скрывает Pager (@CustomPager -> UsersStatisticView.getPager()) если
     * от сервера получен пустой список DTO, или если показывается личная статистика юзера.
     * В противном случае метод делает Pager видимым.
     * Метод необходимо вызывать после обновления данных.
     *
     * @param list список DTO полученных в результате запроса на сервер.
     */
    private void hidePagerIfNecessary(ArrayList<User> list) {
        if (list.isEmpty() || view.isPersonalRatingDataModeSelected()) {
            view.getPager().setVisible(false);
        } else {
            view.getPager().setVisible(true);
        }
    }

    /**
     * Если выбран режим показа линой статистики "показать меня" то строка с
     * целевым DTO объектом выделяется другим цветом (смотреть RatingApplication.selected-user-row)
     * В ином случае возвращаются дефолтные настройки.
     *
     * @param list обновленный список DTO объектов.
     */
    private void updateColorSelection(ArrayList<User> list) {
        if (view.isPersonalRatingDataModeSelected()) {
            view.setColorSelection(list);
        } else {
            view.eraseColorSelection();
        }
    }

    public void updateTotalRowCount() {
        ratingService.getTotalPages(getModel().getSelectedTimeRatingType(), new CommonAsyncCallback<Long>() {
            @Override
            public void onSuccess(Long result) {
                view.getPager().setLabelText(result);
            }
        });
    }

    // TODO limit queries
    public void fillSuggestions(String startsWith) {
        if (startsWith == null || startsWith.length() < RatingConfig.INPUT_STR_LENGTH) {
            return;
        }

        ratingService.findSuggestionsByName(startsWith, model.getSelectedTimeRatingType(),
                new CommonAsyncCallback<ArrayList<String>>() {
                    @Override
                    public void onSuccess(ArrayList<String> result) {
                        model.updateSuggestions(result);

                    }
                }
        );
    }

    public void displaySelectedUser() {
        ratingService.findPageByName(model.getSelectedUserName(), model.getSelectedSortProperty(), model.getSelectedTimeRatingType(),
                new CommonAsyncCallback<ArrayList<User>>() {
                    @Override
                    public void onSuccess(ArrayList<User> result) {
                        updateData(result);
                    }
                }
        );
    }

    public UsersStatisticView getView() {
        if (view == null) {
            view = new UsersStatisticView(UserPresenter.this);
            view.init();
        }
        return view;
    }

    public UserModel getModel() {
        return model;
    }

    public void onUserSelectionChange(User selected) {
        model.setSelectedUserRow(selected);
        PersonalInfoView personalInfoView = new PersonalInfoView(model);
        personalInfoView.showSelectedUser();
    }

}
