package com.javarush.rating.client.presenter;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.Command;
import com.javarush.rating.client.view.WidgetHolder;

public final class WidgetHolderPresenter {

    private final UserPresenter userPresenter;
    private final TaskPresenter taskPresenter;
    private final ChartPresenter chartPresenter;
    private WidgetHolder holder;

    public WidgetHolderPresenter(UserPresenter userPresenter,
                                 TaskPresenter taskPresenter,
                                 ChartPresenter chartPresenter) {
        this.userPresenter = userPresenter;
        this.taskPresenter = taskPresenter;
        this.chartPresenter = chartPresenter;
    }

    public void loadProject() {
        if (holder == null) {
            userPresenter.displayUsersStatistic();
            taskPresenter.displayTaskStatistic();
            chartPresenter.displayChart();
            holder = new WidgetHolder(this);
            Scheduler.get().scheduleDeferred(new Command() {
                @Override
                public void execute() {
                    holder.onFirstLoad();
                }
            });
        }
    }

    public UserPresenter getUserPresenter() {
        return userPresenter;
    }

    public TaskPresenter getTaskPresenter() {
        return taskPresenter;
    }

    public ChartPresenter getChartPresenter() {return chartPresenter;}
}
