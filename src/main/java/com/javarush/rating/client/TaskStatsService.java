package com.javarush.rating.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.javarush.rating.shared.domain.TaskStatsEntity;

import java.util.ArrayList;

/**
* Date: 29.01.14
*
* @author Sergey Kandalintsev
*/
@RemoteServiceRelativePath("springGwtServices/taskStatsService")
public interface TaskStatsService extends RemoteService {
    ArrayList<TaskStatsEntity> findByLevel(int level);
    ArrayList<Integer> findAllLevels();
}
