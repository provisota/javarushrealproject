package com.javarush.rating.shared.helpers;

import com.javarush.rating.shared.domain.RatingEntity;

/**
 * Author: Timur
 * Date: 2/7/14
 */
public final class UserStatistics {

    private final int solvedRating;
    private final int lvlRating;
    private final int avgRating;

    private <T extends RatingEntity> UserStatistics(T rating) {
        this.solvedRating = rating.getSolvedRating();
        this.lvlRating = rating.getLvlRating();
        this.avgRating = rating.getAvgRating();
    }

    public static <T extends RatingEntity> UserStatistics valueOf(T rating) {
        return (rating == null) ? null : new UserStatistics(rating);
    }

    public int getSolvedRating() {
        return solvedRating;
    }

    public int getLvlRating() {
        return lvlRating;
    }

    public int getAvgRating() {
        return avgRating;
    }

    @Override
    public String toString() {
        return "UserStatistics{"
                + "solvedRating=" + solvedRating
                + ", lvlRating=" + lvlRating
                + ", avgRating=" + avgRating
                + '}';
    }
}
