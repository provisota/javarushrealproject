package com.javarush.rating.shared.helpers;

/**
 * Author: Timur
 * Date: 2/8/14
 */
public enum RatingConfig {
    SOLVED {
        @Override
        public int getPage(UserStatistics userStatistics) {
            return (userStatistics.getSolvedRating()-1) / PAGE_SIZE;
        }
    },
    LEVEL {
        @Override
        public int getPage(UserStatistics userStatistics) {
            return (userStatistics.getLvlRating()-1) / PAGE_SIZE;
        }
    },
    AVERAGE {
        @Override
        public int getPage(UserStatistics userStatistics) {
            return (userStatistics.getAvgRating()-1) / PAGE_SIZE;
        }
    };

    public static final int PAGE_SIZE = 10;

    public static final int INPUT_STR_LENGTH = 3;

    public abstract int getPage(UserStatistics userStatistics);
}
