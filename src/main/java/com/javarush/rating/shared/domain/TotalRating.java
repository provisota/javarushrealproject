package com.javarush.rating.shared.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Timur
 */
@Entity
@Table(name = "total_rating")
public class TotalRating extends RatingEntity {

    private static final long serialVersionUID = -2768296695700258035L;

    public TotalRating() {
    }
}
