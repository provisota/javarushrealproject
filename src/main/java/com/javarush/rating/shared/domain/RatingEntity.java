package com.javarush.rating.shared.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author Timur
 */
@MappedSuperclass
public class RatingEntity extends BaseEntity {

    private static final long serialVersionUID = 6512234136218821682L;

    @Column(name = "displayName")
    @NotEmpty
    protected String displayName;

    @Column(name = "pictureUrl")
    @NotEmpty
    protected String pictureUrl;

    @Column(name = "city")
    protected String city;

    @Column(name = "level")
    @NotEmpty
    @Digits(fraction = 0, integer = 3)
    protected Integer level;

    @Column(name = "registered")
    @NotEmpty
    protected Timestamp registered;

    @Column(name = "solved")
    @NotEmpty
    protected Integer solved;

    @Column(name = "average")
    protected BigDecimal average;

    @Column(name = "lvlRating")
    @NotEmpty
    protected Integer lvlRating;

    @Column(name = "solvedRating")
    @NotEmpty
    protected Integer solvedRating;

    @Column(name = "avgRating")
    @NotEmpty
    protected Integer avgRating;

    @Column(name = "hardestTask")
    @NotEmpty
    protected String hardestTask;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Timestamp getRegistered() {
        return registered;
    }

    public void setRegistered(Timestamp registered) {
        this.registered = registered;
    }

    public Integer getSolved() {
        return solved;
    }

    public void setSolved(Integer solved) {
        this.solved = solved;
    }

    public BigDecimal getAverage() {
        return average;
    }

    public void setAverage(BigDecimal average) {
        this.average = average;
    }

    public Integer getLvlRating() {
        return lvlRating;
    }

    public void setLvlRating(Integer lvlRating) {
        this.lvlRating = lvlRating;
    }

    public Integer getSolvedRating() {
        return solvedRating;
    }

    public void setSolvedRating(Integer solvedRating) {
        this.solvedRating = solvedRating;
    }

    public Integer getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Integer avgRating) {
        this.avgRating = avgRating;
    }

    public String getHardestTask() {
        return hardestTask;
    }

    public void setHardestTask(String hardestTask) {
        this.hardestTask = hardestTask;
    }

    @Override
    public String toString() {
        return "RatingEntity{" +
                "displayName='" + displayName + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", city='" + city + '\'' +
                ", level=" + level +
                ", registered=" + registered +
                ", solved=" + solved +
                ", average=" + average +
                ", lvlRating=" + lvlRating +
                ", solvedRating=" + solvedRating +
                ", avgRating=" + avgRating +
                ", hardestTask=" + hardestTask +
                '}';
    }
}
