package com.javarush.rating.shared.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Timur
 */
@Entity
@Table(name = "week_rating")
public class WeekRating extends RatingEntity {

    private static final long serialVersionUID = 6677701669499859902L;

    public WeekRating() {
    }
}
