package com.javarush.rating.shared.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Timur
 */
@Entity
@Table(name = "month_rating")
public class MonthRating extends RatingEntity {

    private static final long serialVersionUID = -5725327577460855885L;

    public MonthRating() {
    }
}
