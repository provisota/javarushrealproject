package com.javarush.rating.shared.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Alterovych Ilya
 */
@Entity
@Table(name = "statistic")
public class StatisticEntity implements Serializable{

    private static final long serialVersionUID = 4127815801735397645L;

    public StatisticEntity() {
    }

    @Id
    @Column(name = "id")
    private Integer taskId;

    @Column(name = "userId")
    private Integer userId;

    @Column(name = "actionType")
    private Integer actionType;

    @Column(name = "message")
    private String message;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "updatedTime")
    private Date updatedTime;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getActionType() {
        return actionType;
    }

    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}
