package com.javarush.rating.shared.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Sergey Kandalintsev
 */
@Entity
@Table(name = "task_stats")
public class TaskStatsEntity implements Serializable {

    private static final long serialVersionUID = -6090750011877457114L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer taskId;

    @Column(name = "keyTask")
    private String keyTask;

    @Column(name = "level")
    private Integer level;

    @Column(name = "usrUnsolved")
    private Integer usrUnsolved;

    @Column(name = "attemptUnsolved")
    private Integer attemptUnsolved;

    @Column(name = "maxAttempts")
    private Integer maxAttempts;

    @Column(name = "usrSolved")
    private Integer usrSolved;

    @Column(name = "attemptSolved")
    private Integer attemptSolved;

    @Column(name = "title")
    private String title;

    public TaskStatsEntity() {
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getKeyTask() {
        return keyTask;
    }

    public void setKeyTask(String keyTask) {
        this.keyTask = keyTask;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }


    public Integer getUsrUnsolved() {
        return usrUnsolved;
    }

    public void setUsrUnsolved(Integer usrUnsolved) {
        this.usrUnsolved = usrUnsolved;
    }

    public Integer getAttemptUnsolved() {
        return attemptUnsolved;
    }

    public void setAttemptUnsolved(Integer attemptUnsolved) {
        this.attemptUnsolved = attemptUnsolved;
    }

    public Integer getMaxAttempts() {
        return maxAttempts;
    }

    public void setMaxAttempts(Integer maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    public Integer getUsrSolved() {
        return usrSolved;
    }

    public void setUsrSolved(Integer usrSolved) {
        this.usrSolved = usrSolved;
    }

    public Integer getAttemptSolved() {
        return attemptSolved;
    }

    public void setAttemptSolved(Integer attemptSolved) {
        this.attemptSolved = attemptSolved;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
