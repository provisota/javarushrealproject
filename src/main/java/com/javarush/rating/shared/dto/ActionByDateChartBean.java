package com.javarush.rating.shared.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Alterovych Ilya
 */
public final class ActionByDateChartBean implements Serializable{

    private static final long serialVersionUID = -4666895297964908494L;

    public ActionByDateChartBean() {
    }

    private Integer actionCount;

    private Date chartDate;

    public Integer getActionCount() {
        return actionCount;
    }

    public void setActionCount(Integer actionCount) {
        this.actionCount = actionCount;
    }

    public Date getChartDate() {
        return chartDate;
    }

    public void setChartDate(Date chartDate) {
        this.chartDate = chartDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("com.javarush.rating.shared.dto.ActionByDateChartBean{");
        sb.append(", actionCount=").append(actionCount);
        sb.append(", chartDate=").append(chartDate);
        sb.append('}');
        sb.append('\n');
        return sb.toString();
    }
}
