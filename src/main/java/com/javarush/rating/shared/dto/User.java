package com.javarush.rating.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.javarush.rating.shared.domain.RatingEntity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;

public final class User implements IsSerializable {

    private Integer id;
    private String displayName;
    private String pictureUrl;
    private String city;
    private Integer level;
    private Timestamp registered;
    private Integer solved;
    BigDecimal average;
    private Integer lvlRating;
    private Integer solvedRating;
    private Integer avgRating;
    private String hardestTask;

    public User() {
    }

    public User(RatingEntity object) {
        this.id = object.getId();
        this.displayName = object.getDisplayName();
        this.pictureUrl = object.getPictureUrl();
        if (object.getCity() != null) {
            this.city = object.getCity();
        }
        this.level = object.getLevel();
        this.registered = object.getRegistered();
        this.solved = object.getSolved();
        this.average = object.getAverage();
        this.lvlRating = object.getLvlRating();
        this.solvedRating = object.getSolvedRating();
        this.avgRating = object.getAvgRating();
        this.hardestTask = object.getHardestTask();
    }

    public static ArrayList<User> convert(ArrayList<? extends RatingEntity> list) {
        ArrayList<User> userList = new ArrayList<User>();
        for (RatingEntity ratingEntity : list) {
            userList.add(new User(ratingEntity));
        }
        return userList;
    }

    public Integer getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public String getCity() {
        return city;
    }

    public Integer getLevel() {
        return level;
    }

    public Timestamp getRegistered() {
        return registered;
    }

    public Integer getSolved() {
        return solved;
    }

    public BigDecimal getAverage() {
        return average;
    }

    public Integer getLvlRating() {
        return lvlRating;
    }

    public Integer getSolvedRating() {
        return solvedRating;
    }

    public Integer getAvgRating() {
        return avgRating;
    }

    public String getHardestTask() {
        return hardestTask;
    }

    @Override
    public String toString() {
        return "User{"
                + "id=" + id
                + ", displayName='" + displayName + '\''
                + ", pictureUrl='" + pictureUrl + '\''
                + ", city='" + city + '\''
                + ", level=" + level
                + ", registered=" + registered
                + ", solved=" + solved
                + ", average=" + average
                + ", lvlRating=" + lvlRating
                + ", solvedRating=" + solvedRating
                + ", avgRating=" + avgRating
                + ", hardestTask='" + hardestTask + '\''
                + '}';
    }
}
