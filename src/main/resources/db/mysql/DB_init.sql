USE rating;

DROP TABLE IF EXISTS total_rating;
DROP TABLE IF EXISTS month_rating;
DROP TABLE IF EXISTS week_rating;
DROP TABLE IF EXISTS task_stats;

CREATE TABLE `total_rating` (
    `id`           INT(11)      NOT NULL,
    `displayName`  VARCHAR(100) NOT NULL,
    `pictureUrl`   VARCHAR(300) NOT NULL,
    `city`         VARCHAR(100) DEFAULT NULL,
    `level`        INT(11)      NOT NULL,
    `registered`   DATETIME     NOT NULL,
    `solved`       INT(11)      NOT NULL DEFAULT '0',
    `average`      DECIMAL(15, 4) DEFAULT NULL,
    `hardestTask`  VARCHAR(30)  NOT NULL,
    `lvlRating`    INT(11)      NOT NULL DEFAULT '0',
    `solvedRating` INT(11)      NOT NULL DEFAULT '0',
    `avgRating`    INT(11)      NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
)
    ENGINE =InnoDB
    DEFAULT CHARSET =utf8;

CREATE TABLE `month_rating` (
    `id`           INT(11)      NOT NULL,
    `displayName`  VARCHAR(100) NOT NULL,
    `pictureUrl`   VARCHAR(300) NOT NULL,
    `city`         VARCHAR(100) DEFAULT NULL,
    `level`        INT(11)      NOT NULL,
    `registered`   DATETIME     NOT NULL,
    `solved`       INT(11)      NOT NULL DEFAULT '0',
    `average`      DECIMAL(15, 4) DEFAULT NULL,
    `hardestTask`  VARCHAR(30)  NOT NULL,
    `lvlRating`    INT(11)      NOT NULL DEFAULT '0',
    `solvedRating` INT(11)      NOT NULL DEFAULT '0',
    `avgRating`    INT(11)      NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
)
    ENGINE =InnoDB
    DEFAULT CHARSET =utf8;

CREATE TABLE `week_rating` (
    `id`           INT(11)      NOT NULL,
    `displayName`  VARCHAR(100) NOT NULL,
    `pictureUrl`   VARCHAR(300) NOT NULL,
    `city`         VARCHAR(100) DEFAULT NULL,
    `level`        INT(11)      NOT NULL,
    `registered`   DATETIME     NOT NULL,
    `solved`       INT(11)      NOT NULL DEFAULT '0',
    `average`      DECIMAL(15, 4) DEFAULT NULL,
    `hardestTask`  VARCHAR(30)  NOT NULL,
    `lvlRating`    INT(11)      NOT NULL DEFAULT '0',
    `solvedRating` INT(11)      NOT NULL DEFAULT '0',
    `avgRating`    INT(11)      NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
)
    ENGINE =InnoDB
    DEFAULT CHARSET =utf8;

CREATE TABLE `task_stats` (
    `id`              INT(11)     NOT NULL DEFAULT '0',
    `keyTask`         VARCHAR(30) NOT NULL,
    `level`           INT(11)     NOT NULL DEFAULT '0',
    `usrUnsolved`     INT(11)     NOT NULL DEFAULT '0',
    `attemptUnsolved` INT(11)     NOT NULL DEFAULT '0',
    `usrSolved`       INT(11)     NOT NULL DEFAULT '0',
    `attemptSolved`   INT(11)     NOT NULL DEFAULT '0',
    `maxAttempts`     INT(11)     NOT NULL DEFAULT '0',
	`title`			  VARCHAR(300) NOT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE =InnoDB
    DEFAULT CHARSET =utf8;

DROP PROCEDURE IF EXISTS updateHelperTables;

DELIMITER $$
CREATE PROCEDURE `updateHelperTables` ()
    BEGIN
        START TRANSACTION;

        DELETE FROM total_rating;
        DELETE FROM month_rating;
        DELETE FROM week_rating;
        DELETE FROM task_stats;

        INSERT INTO total_rating
            SELECT
                U.id,
                U.displayName,
                U.pictureUrl,
                U.city,
                U.level,
                U.createdTime,
                COUNT(UT.status),
                AVG(UT.attempt + 1),
                T.title,
                0,
                0,
                0
            FROM user U
              JOIN usertask UT
                ON U.id = UT.userId
              JOIN (SELECT UTsk.id AS User, tasks.key2 AS title
                    FROM usertask
                      JOIN
                      (SELECT userId AS id, max(attempt) AS attempt
                       FROM usertask
                       GROUP BY userId
                      ) AS UTsk
                        ON usertask.userId = UTsk.ID AND usertask.attempt = UTsk.attempt
                      JOIN tasks
                        ON tasks.id = taskId
                    GROUP BY UTsk.id) AS T
                ON UT.userId = T.User
            WHERE U.level > 10 AND UT.status = 3 AND U.displayName NOT LIKE 'Anonymous #%' AND TRIM(U.displayName) != ''
            GROUP BY U.id;

        SET @row = 0;
        UPDATE total_rating
        SET lvlRating = (@row := @row + 1)
        ORDER BY level DESC, solved DESC, average;

        SET @row = 0;
        UPDATE total_rating
        SET solvedRating = (@row := @row + 1)
        ORDER BY solved DESC, level DESC, average;

        SET @row = 0;
        UPDATE total_rating
        SET avgRating = (@row := @row + 1)
        ORDER BY average, level DESC, solved DESC;

        INSERT INTO month_rating
            SELECT
                U.id,
                U.displayName,
                U.pictureUrl,
                U.city,
                U.level,
                U.createdTime,
                COUNT(UT.status),
                AVG(UT.attempt + 1),
                T.title,
                0,
                0,
                0
            FROM user U
                JOIN usertask UT
                    ON U.id = UT.userId
              JOIN (SELECT UTsk.id AS User, tasks.key2 AS title
                    from usertask
                      JOIN
                      (SELECT userId AS id, max(attempt) AS attempt
                       FROM usertask
                       GROUP BY userId
                      ) AS UTsk
                        ON usertask.userId = UTsk.ID AND usertask.attempt = UTsk.attempt
                      JOIN tasks
                        ON tasks.id = taskId
                    GROUP BY UTsk.id) AS T
                ON UT.userId = T.User
            WHERE U.level > 10 AND UT.status = 3 AND U.displayName NOT LIKE 'Anonymous #%' AND TRIM(U.displayName) != ''
                  AND UT.updatedTime > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
            GROUP BY U.id;

        SET @row = 0;
        UPDATE month_rating
        SET lvlRating = (@row := @row + 1)
        ORDER BY level DESC, solved DESC, average;

        SET @row = 0;
        UPDATE month_rating
        SET solvedRating = (@row := @row + 1)
        ORDER BY solved DESC, level DESC, average;

        SET @row = 0;
        UPDATE month_rating
        SET avgRating = (@row := @row + 1)
        ORDER BY average, level DESC, solved DESC;

        INSERT INTO week_rating
            SELECT
                U.id,
                U.displayName,
                U.pictureUrl,
                U.city,
                U.level,
                U.createdTime,
                COUNT(UT.status),
                AVG(UT.attempt + 1),
                T.title,
                0,
                0,
                0
            FROM user U
                JOIN usertask UT
                    ON U.id = UT.userId
              JOIN (SELECT UTsk.id AS User, tasks.key2 AS title
                    from usertask
                      JOIN
                      (SELECT userId AS id, max(attempt) AS attempt
                       FROM usertask
                       GROUP BY userId
                      ) AS UTsk
                        ON usertask.userId = UTsk.ID AND usertask.attempt = UTsk.attempt
                      JOIN tasks
                        ON tasks.id = taskId
                    GROUP BY UTsk.id) AS T
                ON UT.userId = T.User
            WHERE U.level > 10 AND UT.status = 3 AND U.displayName NOT LIKE 'Anonymous #%' AND TRIM(U.displayName) != ''
                  AND UT.updatedTime > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)
            GROUP BY U.id;

        SET @row = 0;
        UPDATE week_rating
        SET lvlRating = (@row := @row + 1)
        ORDER BY level DESC, solved DESC, average;

        SET @row = 0;
        UPDATE week_rating
        SET solvedRating = (@row := @row + 1)
        ORDER BY solved DESC, level DESC, average;

        SET @row = 0;
        UPDATE week_rating
        SET avgRating = (@row := @row + 1)
        ORDER BY average, level DESC, solved DESC;

        INSERT INTO task_stats
            SELECT
                T.id,
                T.key2,
                T.level,
                SUM(S.usrUnsolved),
                SUM(S.attemptUnsolved),
                SUM(S.usrSolved),
                SUM(S.attemptSolved),
                SUM(S.maxAttempts),
				T.title
            FROM (
                     SELECT
                         T.id,
                         COUNT(DISTINCT UT.userId) AS usrUnsolved,
                         SUM(UT.attempt)           AS attemptUnsolved,
                         0                         AS usrSolved,
                         0                         AS attemptSolved,
                         0                         AS maxAttempts
                     FROM tasks T
                         JOIN usertask UT
                             ON T.id = UT.taskId
                     WHERE UT.status = 2 AND T.key2 NOT LIKE '%type%' AND T.key2 NOT LIKE '%none%'
                     GROUP BY T.id
                     UNION
                     SELECT
                         T.id,
                         0,
                         0,
                         COUNT(DISTINCT UT.userId) AS usrSolved,
                         SUM(UT.attempt + 1)       AS attemptSolved,
                         MAX(UT.attempt + 1)       AS maxAttempts
                     FROM tasks T
                         JOIN usertask UT
                             ON T.id = UT.taskId
                     WHERE UT.status = 3 AND T.key2 NOT LIKE '%type%' AND T.key2 NOT LIKE '%none%'
                     GROUP BY T.id) S
                JOIN tasks T
                    ON S.id = T.id
            GROUP BY T.id, T.key2, T.level, T.title;

        COMMIT;

    END $$
DELIMITER ;

CALL updateHelperTables();









